	<div class='subnavContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span12'>
                        <div id="footerHolder">
                            <div id="ctl00_divModFooter" class="subnav footer-layout-1">
                                <ul class="nav nav-pills pull-left">
                                    <li class="brand-static">KO2 &copy; 2015</li>
                                </ul>
                                <div id="ctl00_divSocialMediaLinks" class="footer-social">
                                    <a id="ctl00_rptSocialMediaLinks_ctl00_lnkSocialLink" class="footer-facebook" href="https://www.facebook.com/pages/Cleveland-Knockouts/1021621231197861" target="_blank"></a>
                                    <a id="ctl00_rptSocialMediaLinks_ctl02_lnkSocialLink" class="footer-twitter" href="https://twitter.com/cleknockouts" target="_blank"></a>
                                    <a id="ctl00_rptSocialMediaLinks_ctl03_lnkSocialLink" class="footer-linkedin" href="https://www.linkedin.com/pub/cleveland-knockouts/b7/112/b3a" target="_blank"></a>
                                    <a id="ctl00_rptSocialMediaLinks_ctl05_lnkSocialLink" class="footer-instagram" href="https://instagram.com/clevelandknockouts/" target="_blank"></a>
                                </div>
                                <div class="nav-footer">
                                    <a class="back-to-top pull-left">Back to Top</a>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                        require(["footerv1"], function(FooterV1) {
                            var FooterV1 = new FooterV1({
                                el: '#footerHolder',
                                SitePageModuleID: "ff948f7e-1896-4408-bd50-50635d7bed91",
                                IsPublish: 1
                            });
                            FooterV1.render();
                        });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style></style>
    <input id="SiteID" type="hidden" value="ac9ddd90-0beb-4fbe-b677-ce09b3996285" />
    <input id="SitePageID" type="hidden" value="15448ab6-730d-428a-b79b-f2485cd7f6f6" />
    <input id="lang" type="hidden" value="en" />
    <div id="fb-root"></div>
    <script type="text/javascript">
    FBAppID = "357097134310266";
    window.fbAsyncInit = function() {
        FB.init({
            appId: FBAppID,
            status: true,
            xfbml: true,
            version: "v2.0"
        });
        FB.Canvas.setSize({
            height: 600
        });
        if (document.getElementById("#big-video-wrap") == null) {
            setTimeout("FB.Canvas.setAutoGrow()", 500);
        }
    };
    (function(d, debug) {
        var js, id = 'facebook-jssdk',
            ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/sdk" + (debug ? "/debug" : "") + ".js";
        ref.parentNode.insertBefore(js, ref);
    }(document, false));
    </script>
</body>

</html>
