<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="SHORTCUT ICON" href="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/955f9568-9d51-4c80-8280-5f62526ece2f.png" />
    <title><?=$page_title;?></title>
    <?php if( !empty( $page_description ) ) { ?>
        <meta name="description" content="<?=$page_description;?>" />
    <?php } ?>
    <?php if( !empty( $page_keywords ) ) { ?>
    <meta name="keywords" content="<?=$page_keywords;?>" />
    <?php } ?>
    <link href="/assets-cloudfront/moprosuite/v10/create/_static2/_css/layout1.css" rel="stylesheet" type="text/css" />
    <link href="/assets-cloudfront/moprosuite/v10/create/_static2/_css/layout2.css" rel="stylesheet" type="text/css" />
    <link href="/assets-cloudfront/moprosuite/v10/create/_static2/_css/layout3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    window.host = {}, host.uikit = "/assets-cloudfront/moprosuite/v10/uikit/_static";
    host.builder = "";
    host.wo = {}, host.wo.web = "", host.wo.static = "/assets-cloudfront/moprosuite/v10/create/_static2";
    host.old_builder = {}, host.old_builder.web = "", host.old_builder.static = "/assets-cloudfront/moprosuite/v10/website/_static";
    window.apiKey = {}, apiKey.Google = "", apiKey.Bitly = "", apiKey.Recaptcha = "", apiKey.Segment = "";
    window.cred = {}, cred.bitly = "";
    window.url = {}, url.getService = {}, url.getService.administration = "";
    url.getService.appsetting = "";
    url.getService.checkout = "";
    url.getService.dashboard = "";
    url.getService.builder = "";
    url.getService.websiteoutput = "";
    url.setService = "";
    url.bitly = "";
    url.api = {}, url.api.ecommerce = "";
    url.api.oauth = "";
    url.api.rewards = "";
    url.api.idx = "";
    window.app = {}, app.lang = "en_US";
    </script>
    <script src="/assets-cloudfront/moprosuite/v10/uikit/_static/_js/lib/require.js" type="text/javascript"></script>
    <script src="/assets-cloudfront/moprosuite/v10/create/_static2/_js/app.js" type="text/javascript"></script>
    <script type='text/javascript' eb src='/assets-typekit/tru5eha.js'></script>
    <script type='text/javascript'>
    try {
        Typekit.load();
    } catch (e) {}
    </script>
</head>
