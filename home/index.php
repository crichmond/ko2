<?php
    $page_title="Home - k02.fit";
    $page_description="Over the past 15 years, Michael Delguyd has built a reputation for being a leader and a pioneer in the personal training and fitness industry. Michael began his journey with an industry giant, Bally's Total Fitness. His scorching and limitless desire to impact the fitness industry along with his aristocratic quest for knowledge helped him breeze pa";
    $page_keywords="fit, fitness, gym, workout, crossfit, strong, training, trainer, diet, girl, weight, personal, shape, pumping, stamina, up, strength, background, light, health, biceps, hands,heavy, bodybuilding, sportswoman, muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic, cross-fit,sportswear, sport";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                         <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem" class="active">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem" class="active">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>

                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_Slider_4786c48d-115e-47fa-9338-89d30b08e471" class="camera_wrap camera_amber_skin mediaslider-layout-1" style="width:100%;height:100%">
            <div id="ctl00_rptSlides_ctl00_imageDiv" data-thumb="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/f8d496fa-6694-4ea8-839b-b20e21023868_t.jpg" data-src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/f8d496fa-6694-4ea8-839b-b20e21023868_h.jpg">
            </div>
            <div id="ctl00_rptSlides_ctl01_imageDiv" data-thumb="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/88403857-eeed-4b21-ad37-2f50ce370c16_t.jpeg" data-src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/88403857-eeed-4b21-ad37-2f50ce370c16_h.jpg">
            </div>
            <div id="ctl00_rptSlides_ctl02_imageDiv" data-thumb="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/5a080fee-163a-44fe-a67f-285d283a86d8_t.jpg" data-src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/5a080fee-163a-44fe-a67f-285d283a86d8_h.jpg">
            </div>
            <div id="ctl00_rptSlides_ctl03_imageDiv" data-thumb="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/1e45c80d-0b61-4426-b93c-8bd2a5cb6309_t.jpg" data-src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/1e45c80d-0b61-4426-b93c-8bd2a5cb6309_h.jpg">
            </div>
            <div id="ctl00_rptSlides_ctl04_imageDiv" data-thumb="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/2afa1966-0d04-43e2-aa89-1ec3af0b18b7_t.jpg" data-src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/2afa1966-0d04-43e2-aa89-1ec3af0b18b7_h.jpg">
            </div>
        </div>
        <input name="ctl00$txtMediaSetting_4786c48d-115e-47fa-9338-89d30b08e471" type="hidden" id="ctl00_txtMediaSetting_4786c48d-115e-47fa-9338-89d30b08e471" value="{&quot;Height&quot;:30,&quot;ImageAlignment&quot;:&quot;&quot;,&quot;NavStyle&quot;:&quot;none&quot;,&quot;NavButton&quot;:0,&quot;NavPosition&quot;:&quot;&quot;,&quot;NavAlignment&quot;:&quot;&quot;,&quot;TranStyle&quot;:&quot;curtainTopLeft&quot;,&quot;TranTime&quot;:0,&quot;TranLoop&quot;:&quot;loop&quot;,&quot;IconID&quot;:&quot;00000000-0000-0000-0000-000000000000&quot;,&quot;IconURL&quot;:&quot;&quot;,&quot;Title&quot;:&quot;&quot;,&quot;Subtitle&quot;:&quot;&quot;,&quot;ShadowLip&quot;:0,&quot;ImageSizeTypeID&quot;:1300,&quot;SeqNo&quot;:4,&quot;ID&quot;:&quot;2dfdd645-3621-4369-83fb-00aa4376d05c&quot;,&quot;Type&quot;:&quot;ContentBO.Content.MediaSliderRow&quot;,&quot;IsNew&quot;:false}" />
        <script type="text/javascript" language="javascript">
        require(["mediasliderv1"], function(MediaSlider) {
            var MediaSlider = new MediaSlider({
                SitePageModuleID: "4786c48d-115e-47fa-9338-89d30b08e471",
                StaticUrl: "/assets-cloudfront/moprosuite/v10/create/_static2/_css/_images/",
                IsPublish: 1
            });
        });
        </script>
    </div>
    <div class='centerContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span12'>
                        <?php if( $_COOKIE["/endpoints/first-session/"] != true ) { ?>
                            <script type="text/javascript">
                            require(["overlayv1"], function(OverlayV1) {
                                OverlayV1.loadModule(1, "/endpoints/page-over-relay", 0, 50, 0, 0);
                                window.OverlayV1 = OverlayV1;
                            });
                            </script>
                        <?php } ?>
                        <div class="mod-embed-iframe embed-layout-1">
                            <div class="mod-embed-iframe">
                                <style>
                                .centerContainer {
                                    background: #000000;
                                }
                                </style>
                            </div>
                        </div>
                        <div id="modRichTextEditor" class="mod-rte rte-layout-1">
                            <div id="txtDescription" class="mod-text-wrapper">
                                <h3 class="align-center">&nbsp;</h3>
                                <h3 class="align-center"><span class="bold">FITNESS WITH A FOCUS</span></h3>
                                <p class="align-center"><span class="bold">OXYGEN-ENRICHED ENVIRONMENT &bull;&nbsp;TEAM WORKOUTS NEXT GENERATION EQUIPMENT </span>
                                    <br /><span class="bold">BURN 600 - 1000 CALORIES IN A SINGLE SESSION STRESS RELIEVING WORKOUTS THAT ACCOMMODATE AGE AND EXPERIENCE.</span></p>
                            </div>
                        </div>
                        <div id="ctl00_divModButtonLink" class="mod-call-to-action clearfix mod_btnlink_f6ef3ad8-22ef-420d-b336-9a1eeae1f506">
                            <div class="art-reward-points"><a href="contact" id="ctl00_lnkButtonLink" class="btn btn-centered-cls">One free session</a>
                            </div>
                        </div>
                        <script type="text/javascript">
                        require(["buttonlinkv1"], function(ButtonLinkV1) {
                            var ButtonLinkV1 = new ButtonLinkV1({
                                //el: '#divModButtonLink',
                                SitePageModuleID: "f6ef3ad8-22ef-420d-b336-9a1eeae1f506",
                                IsPublish: 1
                            });
                            ButtonLinkV1.render();
                        });
                        </script>
                        <div id="ctl00_paddingWrapper" class="mod-padding hide-space-mobile" style="height: 35px;"></div>
                        <style type="text/css">

                        </style>
                        <div id="ctl00_divClientele" class="mod-clientele mod-clientele-1fbb30b1-2c99-4384-8d2b-84e753d96d07 clientele-layout-1 item-per-row-2">
                            <div id="ctl00_rptTeam_ctl00_divClient" class="clientele-cards row-start">
                                <div class="front-card">
                                    <div class="clientele-image-wrap">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/4cf264ba-09f7-4c45-abbf-b79ba19cc9eb_l.jpg" id="imgClientPhotoFront" alt="Client Front Image" class="clientele-absimg" />
                                    </div>
                                </div>
                                <div class="back-card">
                                    <div class="client-header">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/4cf264ba-09f7-4c45-abbf-b79ba19cc9eb_t.jpg" id="imgClientPhotoBack" alt="Client Front Image" />
                                        <div class="client-header-text">
                                            <h3 id="h3Title">KO2 Core</h3>
                                        </div>
                                    </div>
                                    <div class="client-footer">
                                        <p id="pDescription">Your core is the foundation to your body. 18 minutes of working out with our gladiator walls will build a solid foundation to the body you&#39;ve always dreamed of and much more. This H.I.R.T can also help recover injuries through kinesiology.</p>
                                        <a id="aURL" class="clientele-link" href="about"></a>
                                    </div>
                                </div>
                            </div>
                            <div id="ctl00_rptTeam_ctl01_divClient" class="clientele-cards clientele-alt row-end">
                                <div class="front-card">
                                    <div class="clientele-image-wrap">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/3f7f9665-1299-445b-b6f7-7baa2ce990f6_l.jpg" id="imgClientPhotoFront" alt="Client Front Image" class="clientele-absimg" />
                                    </div>
                                </div>
                                <div class="back-card">
                                    <div class="client-header">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/3f7f9665-1299-445b-b6f7-7baa2ce990f6_t.jpg" id="imgClientPhotoBack" alt="Client Front Image" />
                                        <div class="client-header-text">
                                            <h3 id="h3Title">KO2 Tone</h3>
                                        </div>
                                    </div>
                                    <div class="client-footer">
                                        <p id="pDescription">18 minutes of toning with our NuBells will build a balance of long, lean muscle throughout your body. For every pound of lean muscle built your body will burn 50 extra calories per day at rest.</p>
                                        <a id="aURL" class="clientele-link" href="core-tone-box"></a>
                                    </div>
                                </div>
                            </div>
                            <div id="ctl00_rptTeam_ctl02_divClient" class="clientele-cards row-start">
                                <div class="front-card">
                                    <div class="clientele-image-wrap">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/d8d4bb7d-ab87-4805-95b1-e13792868ad0_l.jpg" id="imgClientPhotoFront" alt="Client Front Image" class="clientele-absimg" />
                                    </div>
                                </div>
                                <div class="back-card">
                                    <div class="client-header">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/d8d4bb7d-ab87-4805-95b1-e13792868ad0_t.jpg" id="imgClientPhotoBack" alt="Client Front Image" />
                                        <div class="client-header-text">
                                            <h3 id="h3Title">KO2 Boxing</h3>
                                        </div>
                                    </div>
                                    <div class="client-footer">
                                        <p id="pDescription">18 minutes of boxing combines the excitement and stress release of striking combos with the body sculpting burn of high intensity interval training. Focus master is fun to punch, knee and kick. By the end of your workout you will feel like Mike Tyson, but please, refrain from any ear biting.</p>
                                        <a id="aURL"></a>
                                    </div>
                                </div>
                            </div>
                            <div id="ctl00_rptTeam_ctl03_divClient" class="clientele-cards clientele-alt row-end">
                                <div class="front-card">
                                    <div class="clientele-image-wrap">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/9e579b7b-fdbe-462d-96b6-f7ffa812784d_l.jpg" id="imgClientPhotoFront" alt="Client Front Image" class="clientele-absimg" />
                                    </div>
                                </div>
                                <div class="back-card">
                                    <div class="client-header">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/9e579b7b-fdbe-462d-96b6-f7ffa812784d_t.jpg" id="imgClientPhotoBack" alt="Client Front Image" />
                                        <div class="client-header-text">
                                            <h3 id="h3Title">KO2 Zone</h3>
                                        </div>
                                    </div>
                                    <div class="client-footer">
                                        <p id="pDescription">By tracking your progress in real time, you will be able to see the breakdown of your exercise efforts. Tracking your progress over time will help you gain understanding in your workout habits. MyZone will help push you to the limit and reach goals you would have never thought possible.</p>
                                        <a id="aURL"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["clientelev1"], function(ClienteleV1) {
                            var ClienteleV1 = new ClienteleV1({
                                SitePageModuleID: "1fbb30b1-2c99-4384-8d2b-84e753d96d07",
                                IsPublish: 1
                            });
                        });
                        </script>
                        <div class="mod-embed-iframe embed-layout-1">
                            <div class="mod-embed-iframe">
                                <style>
                                .centerContainer .btn {
                                    color: #000!important;
                                    background: #5eeb3b!important;
                                    background-color: rgba(94, 235, 59, 1)!important;
                                }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>