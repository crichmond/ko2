<?php
    $page_title="Social - k02.fit";
    $page_description="What does the +1 Mindset mean? +1 is what happens every time you perform a kind act to make someone else's life better or a little less stressful.";
    $page_keywords="training, trainer, diet, girl, weight, personal, shape, pumping, stamina, up, strength, background, light, health, biceps, hands,heavy, bodybuilding, sportswoman, muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                         <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown active">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle active">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_divModParallax" class="mod-parallax mod-parallax-82d7e017-36e2-4938-9308-28fc72ce7df7 parallax-layout-2 padBg" data-speed="10" sitepagemoduleid="82d7e017-36e2-4938-9308-28fc72ce7df7" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" style="max-height: 500px;">
            <img id="ctl00_bgParallaxImg" class="scroll-Parallax-image" sitepagemoduleid="82d7e017-36e2-4938-9308-28fc72ce7df7" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            <div>
                <img id="ctl00_invisibleParallaxImg" class="hidden-Parallax-image" sitepagemoduleid="82d7e017-36e2-4938-9308-28fc72ce7df7" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            </div>
            <div class="parallax-text-wrap">
                <div class="parallax-text-table">
                    <div id="ctl00_divHeader" class="parallaxtext">
                        <h3 id="ctl00_litParallaxTitle">Social</h3>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["jquery", "imageparallaxv1"], function($, ImageParallaxV1) {
            ImageParallaxV1.loadModule("82d7e017-36e2-4938-9308-28fc72ce7df7");
        });
        </script>
    </div>
    <div class='centerContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span12'>
                        <style type="text/css">

                        </style>
                        <div id="ctl00_divSocialwall" class="mod-socialwall mod-socialwall-1ba082e1-ef4a-47f9-982a-f8a84c627713">
                            <div id="masonry-fit-width">
                                <div id="socialFilters" class="theFilters" style="text-align:center;">
                                    <a id="ctl00_allfilter" class="filter filterblock hide" rel="filterAll">
                                        <div class="filter filterAll active">
                                        </div>
                                    </a>
                                    <a id="ctl00_facebookfilter" class="filter filterblock hide" rel="filterFacebook">
                                        <div class="filter filterFacebook">
                                        </div>
                                    </a>
                                    <a id="ctl00_instagramfilter" class="filter filterblock hide" rel="filterInstagram">
                                        <div class="filter filterInstagram">
                                        </div>
                                    </a>
                                    <a id="ctl00_twitterfilter" class="filter filterblock hide" rel="filterTwitter">
                                        <div class="filter filterTwitter">
                                        </div>
                                    </a>
                                    <div id="social-spinner" class="socialSpinner">
                                    </div>
                                </div>
                                <div id="isotopeContainer" class="isotope">
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["socialwallv1"], function(SocialWallV1) {
                            var SocialWallV1 = new SocialWallV1({
                                SitePageModuleID: "1ba082e1-ef4a-47f9-982a-f8a84c627713",
                                //Facebook
                                FacebookPageID: "1021621231197861",
                                FacebookPageURL: "https://www.facebook.com/pages/Cleveland-Knockouts/1021621231197861",
                                //Pinterest
                                PinterestPageID: "",
                                PinterestPageURL: "",
                                //Instagram
                                InstagramPageID: "1929648845",
                                InstagramClientID: "ccb6235da40e4f11b5ba53642ebf2456",
                                InstagramPageURL: "https://instagram.com/clevelandknockouts/",
                                //Google+
                                GooglePlusPageID: "",
                                GooglePlusPageURL: "",
                                //YouTube
                                YouTubePageID: "",
                                YouTubePageURL: "",
                                //Twitter
                                TwitterPageID: "cleknockouts",
                                TwitterPageURL: "https://twitter.com/cleknockouts",
                                //LoyalityPoints
                                FacebookPoints: "",
                                TwitterPoints: "",
                                LinkedInPoints: "",
                                retrydelay: "",
                                isPointDisplay: 1
                            });
                        });
                        //    /*
                        //    **************************************
                        //    initializing isotope and adding settings
                        //    **************************************
                        //    */
                        //    $(function () {
                        //        var mynodes = $('.isotope').isotope({
                        //            itemSelector: '.item',
                        //            masonry: {
                        //                columnWidth: 220,
                        //                gutter: 20,
                        //                isFitWidth: true
                        //            }
                        //        });
                        //    });
                        /*
                        **************************************
                        fading in and out the post title/text on hover
                        **************************************
                        */
                        //    $(document).ready(function () {
                        //        var thePost = $('.postWrapper');
                        //        var shareLinks = $('.shareIconsContainer');
                        //        var hoverText = $('.viewPost');
                        //        var clockText = $('.timestamp.clockText');
                        //        var theClock = $('.clock');
                        //        $('.itemImageContainer').hover(function () {
                        //            $(this).parent().find(thePost).addClass('fadeOutDown');
                        //            $(this).parent().find(thePost).removeClass('fadeInUp');
                        //            $(this).parent().find(shareLinks).addClass('fadeInUp');
                        //            $(this).parent().find(shareLinks).removeClass('fadeOutDown');
                        //        }, function () {
                        //            $(this).parent().find(thePost).removeClass('fadeOutDown');
                        //            $(this).parent().find(thePost).addClass('fadeInUp');
                        //            $(this).parent().find(shareLinks).removeClass('fadeInUp');
                        //            $(this).parent().find(shareLinks).addClass('fadeOutDown');
                        //        });
                        //        /*
                        //        **************************************
                        //        filtering isotope (items)
                        //        **************************************
                        //        */
                        //        $('.filter').bind('click', function () {
                        //            var to_filter = $(this).attr('rel');
                        //            var iso = $('.isotope');
                        //            $('.filter').removeClass('active');
                        //            switch (to_filter) {
                        //                case "filterFacebook":
                        //                    iso.isotope({ filter: '.facebook' });
                        //                    $('.filterFacebook').addClass('active');
                        //                    break;
                        //                case "filterInstagram":
                        //                    iso.isotope({ filter: '.instagram' });
                        //                    $('.filterInstagram').addClass('active');
                        //                    break;
                        //                case "filterPinterest":
                        //                    iso.isotope({ filter: '.pinterest' });
                        //                    $('.filterPinterest').addClass('active');
                        //                    break;
                        //                case "filterAll":
                        //                    iso.isotope({ filter: '.item' });
                        //                    break;
                        //                default:
                        //                    iso.isotope({ filter: '.item' });
                        //                    break;
                        //            }
                        //        });
                        //        /*
                        //        **************************************
                        //        bouncing hover state elements
                        //        **************************************
                        //        */
                        //        $('.share').hover(function () {
                        //            $(this).addClass('bounce');
                        //        }, function () {
                        //            $(this).removeClass('bounce');
                        //        });
                        //        /*
                        //        **************************************
                        //        checking if mobile device/touch device
                        //        **************************************
                        //        */
                        //        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                        //            $('.timestamp').hide();
                        //            $('.viewPostMobile').show();
                        //            $('.viewPostText').hide();
                        //        }
                        //        /*
                        //        **************************************
                        //        checking browser version via conditional comments
                        //        creating fallbacks for ie8, ie9 and firefox
                        //        **************************************
                        //        */
                        //        if ($("html").hasClass("ie8")) {
                        //            $('.itemImage').css({ backgroundSize: 'cover' });
                        //            $('.itemImage').css('z-index', '0');
                        //            var thePost = $('.postWrapper');
                        //            var shareLinks = $('.shareIconsContainer');
                        //            if ($(window).width() < 480) {
                        //                thePost.css('height', '60px');
                        //                $('.share').css('height', '25px');
                        //                $('.share').css('width', '25px');
                        //                $('.socialLine').css('height', '25px');
                        //                $('.item').css('width', '100%');
                        //                $('.item').css('height', '220px');
                        //                $('.postTitle').css('font-size', '12px');
                        //                $('.isotope').css('margin', '0');
                        //                $('.isotope').css('width', '100%');
                        //            }
                        //            $(window).resize(function () {
                        //                if ($(window).width() < 480) {
                        //                    thePost.css('height', '60px');
                        //                    $('.share').css('height', '25px');
                        //                    $('.share').css('width', '25px');
                        //                    $('.socialLine').css('height', '25px');
                        //                    $('.item').css('width', '100%');
                        //                    $('.item').css('height', '220px');
                        //                    $('.postTitle').css('font-size', '12px');
                        //                    $('.isotope').css('margin', '0');
                        //                    $('.isotope').css('width', '100%');
                        //                }
                        //            });
                        //            $('.postWrapper').css('background', 'black !important');
                        //            $('.share').css('background', 'none');
                        //            $('.theNetwork').css('background', 'none');
                        //            $('.shareIconsContainer').hide();
                        //            $('.itemImageContainer').hover(function () {
                        //                $(this).parent().find(shareLinks).show();
                        //                $(this).parent().find(thePost).hide();
                        //            }, function () {
                        //                $(this).parent().find(shareLinks).hide();
                        //                $(this).parent().find(thePost).show();
                        //            });
                        //        }
                        //        if ($("html").hasClass("ie9")) {
                        //            var thePost = $('.postWrapper');
                        //            var shareLinks = $('.shareIconsContainer');
                        //            $('.shareIconsContainer').removeClass('visibilityNone');
                        //            $('.shareIconsContainer').hide();
                        //            $('.itemImageContainer').hover(function () {
                        //                $(this).parent().find(shareLinks).show();
                        //                $(this).parent().find(thePost).hide();
                        //            }, function () {
                        //                $(this).parent().find(shareLinks).hide();
                        //                $(this).parent().find(thePost).show();
                        //            });
                        //        }
                        //    });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>