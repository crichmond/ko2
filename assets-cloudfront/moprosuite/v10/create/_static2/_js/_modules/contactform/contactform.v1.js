define("contactformv1", ["domReady", "jquery", "utility"], function(c, a, d) {
    var b = {
        isFormValidated: true,
        SiteID: null,
        loadModule: function() {},
        validateContactFormFields: function(e) {
            isFormValidated = true;
            a(e).find("[data-required=true]").each(function(h, g) {
                if (a.trim(a(g).getWaterValue()) == "") {
                    a(a(g).data("parentid")).addClass("error");
                    isFormValidated = false
                }
            });
            var f = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var f2 = /^1?\W*([2-9][0-8][0-9])\W*([2-9][0-9]{2})\W*([0-9]{4})(\se?x?t?(\d*))?$/i;
            a(e).find("[data-validation=email]").each(function(h, g) {
                if (!f.test(a.trim(a(g).getWaterValue()))) {
                    a(a(g).data("parentid")).addClass("error");
                    isFormValidated = false
                }
            });
            a(e).find("[data-validation=phone]").each(function(h, g) {
                if (!f2.test(a.trim(a(g).getWaterValue()))) {
                    a(a(g).data("parentid")).addClass("error");
                    isFormValidated = false
                }
            });

            return isFormValidated
        },
        sendEmail: function(g, f, i) {
            var h = this;
            h.SiteID = a("input#SiteID").val();
            var e = a(f).closest(".mod-contact");
            var url = e.attr( "action" );
            var method = e.attr( "method" );
            d.xStopPropagation(g);
            if (h.validateContactFormFields(e)) {
                d.Proxy.send({
                    options: {},
                    url: url,
                    datatype: "json",
                    data: {
                        SitePageModuleID: i,
                        SiteID: h.SiteID,
                        name: e.find("input[id$=txtName]").getWaterValue(),
                        email: e.find("input[id$=txtEmail]").getWaterValue(),
                        subject: e.find("input[id$=txtSubject]").getWaterValue(),
                        message: e.find("textarea[id$=txtMessage]").getWaterValue()
                    },
                    method: method,
                    crossDomain: false,
                    beforeSend: a.noop(),
                    onSuccess: function(k, j) {
                        if (k[0].MSGSent == "true") {
                            e.find("input:text, textarea, input[type='tel']").each(function(n, m) {
                                a(m).val("")
                            });
                            e.find(".divContactFormSuccessMessage").show();
                            h.traceEvent(f);
                            var l = setTimeout(function() {
                                e.find(".divContactFormSuccessMessage").hide()
                            }, 3000)

                            function createCookie(name, value, days) {
                                var expires;

                                if (days) {
                                    var date = new Date();
                                    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                                    expires = "; expires=" + date.toGMTString();
                                } else {
                                    expires = "";
                                }
                                document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
                            }
                            createCookie(url,true,999);
                        } else {
                            e.find(".divContactFormFailureMessage").show()
                        }
                    },
                    onError: function(k, j) {
                        e.find(".divContactFormFailureMessage").show()
                    }
                })
            }
        },
        removeError: function(e) {
            a(a(e).data("parentid")).removeClass("error")
        },
        applyWatermarks: function() {
            var e = a("[id$=divModContactForm]");
            if (e.length > 0 && e.hasClass("contactform-layout-5")) {
                a(e).Watermarkify({
                    className: "watermark"
                })
            }
        },
        traceEvent: function(e) {
            var f = this;
            if (typeof(d) != "undefined" && d.Tracker != undefined && typeof(d.Tracker) == "function") {
                var h = a("[id$=SitePageID]").val();
                var g = Math.floor(Math.random() * (100 - 1)) + 1;
                d.Tracker(17330, h, "", 1162, true);
                Header.triggerLoyalityRewardPoints("[id$=spnContactFormPoints]", "[id$=aSubmit]", 17330)
            }
        }
    };
    c(function() {
        b.applyWatermarks()
    });
    return b
});
