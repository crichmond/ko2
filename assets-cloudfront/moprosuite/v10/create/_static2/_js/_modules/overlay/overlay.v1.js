define("overlayv1", ["jquery", "backbone", "utility", "modal"], function(a, b, c, d) {
    var e = {
        Overlay: null,
        loadModule: function(j, l, h, m, k, g) {
            var f = this;
            if (!c.isBlank(l)) {
                f.OpenCounter = window.setTimeout(function() {
                    f.Overlay = new d({
                        modalType: "dialog",
                        autoOpen: true,
                        showOverlay: true,
                        title: "",
                        subtitle: "",
                        description: "",
                        width: (parseInt(a(window).width(), 10) * parseInt(m, 10) / 100),
                        className: "overlay-v1",
                        buttons: [],
                        content: {
                            url: l,
                            datatype: "html",
                            onSuccess: function(o, n) {
                                n.el.html(o)
                            },
                            onError: function(o, n) {
                                f.Overlay.doClose()
                            }
                        },
                        afterContentLoad: function() {
                            if (g > 0) {
                                f.CloseCounter = window.setTimeout(function() {
                                    f.Overlay.doClose()
                                }, (parseInt(g, 10) * 1000))
                            }
                        }
                    })
                }, (parseInt(k, 10) * 1000))
            }
        }
    };
    return e
});
