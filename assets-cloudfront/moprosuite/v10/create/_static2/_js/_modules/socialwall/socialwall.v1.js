define("socialwallv1", ["jquery", "backbone", "utility", "data", "isotope", "text!/tpl/socialwall/?tpl=/_tpl/modules/socialwall/v1/tile.html&host=websiteoutput"], function(a, b, e, f, d, n) {
    var c = parseInt(new Date().getTime(), 10);
    var h = b.Model.extend({
        defaults: {
            id: "",
            page_name: "",
            wallname: "",
            title: "",
            image: "",
            post_url: "",
            description: "",
            likecount: "",
            commentcount: "",
            posteddate: "",
            postedtimediff: "",
            isprime: false,
            page_url: "",
            rendered: false,
            isDuplicate: false,
            fbPoints: "",
            twitterPoints: "",
            linkedInPoints: "",
            retrydelay: "",
            isPointDisplay: 1
        },
        initialize: function() {
            var o = c - this.get("posteddate");
            o = o / 1000;
            o = o / 60;
            if (o > 60) {
                o = o / 60;
                if (o > 24) {
                    o = o / 24;
                    if (o > 30) {
                        o = o / 30;
                        if (o > 12) {
                            o = o / 12;
                            if (o > 10) {
                                this.set("postedtimediff", "10+ y ago")
                            } else {
                                this.set("postedtimediff", Math.floor(o) + " y ago")
                            }
                        } else {
                            this.set("postedtimediff", Math.floor(o) + " mo ago")
                        }
                    } else {
                        this.set("postedtimediff", Math.floor(o) + " d ago")
                    }
                } else {
                    this.set("postedtimediff", Math.floor(o) + " h ago")
                }
            } else {
                this.set("postedtimediff", Math.floor(o) + " mins ago")
            }
            this.set("likecount", parseInt(this.get("likecount")))
        }
    });
    var j = b.Collection.extend({
        model: h
    });
    var k = b.Collection.extend({
        model: h
    });
    var g = b.Collection.extend({
        model: h
    });
    var m = b.View.extend({
        el: null,
        socialCollection: null,
        iso: null,
        browserType: "",
        fbRequestSuccess: true,
        pinRequestSuccess: true,
        instaRequestSuccess: true,
        gplusRequestSuccess: true,
        youtubeRequestSuccess: true,
        twitterRequestSuccess: true,
        defaultOptions: {
            SitePageModuleID: null,
            FacebookPageID: "",
            PinterestPageID: "",
            InstagramPageID: "",
            InstagramClientID: "",
            FacebookPageURL: "",
            PinterestPageURL: "",
            InstagramPageURL: "",
            GooglePlusPageID: "",
            GooglePlusPageURL: "",
            YouTubePageID: "",
            YouTubePageURL: "",
            TwitterPageID: "",
            TwitterPageURL: "",
            FacebookPoints: "",
            TwitterPoints: "",
            LinkedInPoints: "",
            retrydelay: "",
            isPointDisplay: 1
        },
        events: {
            "click .filterblock": "filterFeeds"
        },
        initialize: function() {
            var o = this;
            o.setElement(".mod-socialwall-" + o.options.SitePageModuleID);
            o.socialCollection = new j();
            o.socialCollection.comparator = function(p) {
                return -p.get("posteddate")
            };
            o.socialOrderedCollection = new k();
            o.socialOrderedCollection.comparator = function(p) {
                return -p.get("likecount")
            };
            o.pinterestTempCollection = new g();
            o.pinterestTempCollection.comparator = function(p) {
                return -p.get("posteddate")
            };
            o.browserSpecific();
            o.render();
            a(window).bind("resize", {
                me: o
            }, o.resize)
        },
        resize: function(o) {
            var p = o.data.me;
            if (p.browserType == "ie8" && a(window).width() < 480) {
                p.$el.find(".isotope").toggleClass("ieToggleSmall", true)
            } else {
                p.$el.find(".isotope").toggleClass("ieToggleSmall", false)
            }
        },
        browserSpecific: function() {
            var p = this;
            if ((/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))) {
                p.$el.addClass("mobileController")
            }
            var q = navigator.userAgent.toLowerCase(),
                o = "",
                r = 0;
            if (q.toLowerCase().indexOf("msie 9.0;") >= 0) {
                p.browserType = "ie9"
            } else {
                if (q.toLowerCase().indexOf("msie 8.0;") >= 0) {
                    p.browserType = "ie8"
                }
            }
            if (p.$el.hasClass("browserie8")) {
                p.browserType = "ie8"
            } else {
                if (p.$el.hasClass("browserie9")) {
                    p.browserType = "ie9"
                }
            }
            if (p.browserType == "ie8") {
                p.$el.find(".isotope").addClass("ieToggle");
                require(["backgroundie8"], function() {
                    a(".itemImage").css({
                        backgroundSize: "cover"
                    })
                });
                if (a(window).width() < 480) {
                    p.$el.find(".isotope").toggleClass("ieToggleSmall", true)
                } else {
                    p.$el.find(".isotope").toggleClass("ieToggleSmall", false)
                }
            }
        },
        render: function() {
            var o = this;
            o.$el.find("#social-spinner").show();
            o.$el.find("#social-spinner").ActivityIndicator();
            o.doSocialRequests();
            o.socialCollection.each(function(p) {
                var q = _.template(n, p.toJSON());
                o.$el.append(q)
            })
        },
        doSocialRequests: function() {
            var p = this;
            if (!e.isBlank(p.options.FacebookPageID)) {
                var o = "FacebookPageID=" + p.options.FacebookPageID;
                p.fbdata = new f.Collection([], {
                    host: "websiteoutput",
                    method: "GetFBClientFeedResponse",
                    params: o,
                    typefields: [{
                        DataType: "BOCollection",
                        Columns: "*"
                    }]
                });
                p.fbRequestSuccess = false;
                p.fbdata.fetch({
                    success: function(s, t, r) {
                        p.fbRequestSuccess = true;
                        if (s.length > 0) {
                            p.fbdata.each(function(v, w) {
                                var y = c;
                                if (p.browserType == "ie8") {
                                    y = parseInt(new Date(v.get("created_time")).getTime(), 10)
                                } else {
                                    y = parseInt(new Date(v.get("created_time")).getTime(), 10)
                                }
                                var x = v.get("feedid");
                                if (x != "") {
                                    x = x.split("_");
                                    if (x.length > 1) {
                                        x = "https://www.facebook.com/" + p.options.FacebookPageID + "/posts/" + x[1]
                                    } else {
                                        x = v.get("post_url")
                                    }
                                }
                                if (a.trim(v.get("message")) != "" || a.trim(v.get("picture")) != "") {
                                    var u = new h({
                                        id: v.get("feedid"),
                                        page_name: v.get("name"),
                                        wallname: "facebook",
                                        title: v.get("message"),
                                        image: v.get("picture"),
                                        description: "",
                                        post_url: x,
                                        likecount: v.get("like_cnt"),
                                        commentcount: v.get("comment_cnt"),
                                        posteddate: y,
                                        page_url: p.options.FacebookPageURL,
                                        isprime: false,
                                        istext: (a.trim(v.get("picture")) == "" && a.trim(v.get("message")) != ""),
                                        fbPoints: p.options.FacebookPoints,
                                        twitterPoints: p.options.TwitterPoints,
                                        linkedInPoints: p.options.LinkedInPoints,
                                        retrydelay: p.options.retrydelay,
                                        isPointDisplay: p.options.isPointDisplay
                                    });
                                    p.socialCollection.add(u);
                                    p.$el.find("#socialFilters").removeClass("hide");
                                    p.$el.find(".filter[rel=filterAll]").removeClass("hide");
                                    p.$el.find(".filter[rel=filterFacebook]").removeClass("hide")
                                }
                            });
                            p.shufflePosts();
                            p.updateFBPhotos()
                        }
                        p.removeLoadingSpinner()
                    },
                    error: function(r, s) {}
                })
            }
            if (!e.isBlank(p.options.PinterestPageID)) {
                p.pinRequestSuccess = false;
                var q = a.ajax({
                    type: "GET",
                    url: "https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=30&callback=?&q=" + encodeURIComponent("https://www.pinterest.com/" + p.options.PinterestPageID + "/feed.rss"),
                    dataType: "jsonp",
                    success: function(u) {
                        if (u.responseData != null) {
                            var r = u.responseData.feed.entries;
                            var t = [];
                            p.pinRequestSuccess = true;
                            a.each(r, function(w, x) {
                                if (w < 10) {
                                    var v = "<div>" + x.content + "</div>";
                                    var y = "";
                                    var z = x.link.match(/\/pin\/(\d+)/);
                                    if (z != null && z.length == 2) {
                                        y = z[1];
                                        var A = new h({
                                            id: y,
                                            wallname: "pinterest",
                                            title: x.contentSnippet,
                                            image: a(v).find("img:first").attr("src").replace("236", "736"),
                                            description: x.contentSnippet,
                                            post_url: unescape(x.link),
                                            likecount: 0,
                                            commentcount: 0,
                                            posteddate: parseInt(new Date(x.publishedDate).getTime(), 10),
                                            page_url: p.options.PinterestPageURL,
                                            isprime: false,
                                            istext: false,
                                            fbPoints: p.options.FacebookPoints,
                                            twitterPoints: p.options.TwitterPoints,
                                            linkedInPoints: p.options.LinkedInPoints,
                                            retrydelay: p.options.retrydelay,
                                            isPointDisplay: p.options.isPointDisplay
                                        });
                                        p.pinterestTempCollection.add(A);
                                        t.push(y)
                                    }
                                }
                            });
                            var s = a.ajax({
                                type: "GET",
                                url: "https://api.pinterest.com/v3/pidgets/pins/info/?pin_ids=" + t,
                                dataType: "jsonp",
                                success: function(w) {
                                    var v = w.data;
                                    a.each(v, function(x, y) {
                                        var z = p.pinterestTempCollection.where({
                                            id: y.id
                                        });
                                        if (z.length > 0) {
                                            z[0].set("likecount", y.like_count)
                                        }
                                    });
                                    p.pinterestTempCollection.each(function(z, x, y) {
                                        p.socialCollection.add(z);
                                        p.$el.find("#socialFilters").removeClass("hide");
                                        p.$el.find(".filter[rel=filterAll]").removeClass("hide");
                                        p.$el.find(".filter[rel=filterPinterest]").removeClass("hide")
                                    });
                                    p.pinterestTempCollection.reset();
                                    p.shufflePosts();
                                    p.removeLoadingSpinner()
                                }
                            })
                        } else {
                            p.pinRequestSuccess = true;
                            p.removeLoadingSpinner()
                        }
                    }
                })
            }
            if (!e.isBlank(p.options.InstagramPageID)) {
                p.instaRequestSuccess = false;
                var q = a.ajax({
                    type: "GET",
                    url: "https://api.instagram.com/v1/users/" + p.options.InstagramPageID + "/media/recent/?client_id=" + p.options.InstagramClientID,
                    dataType: "jsonp",
                    success: function(s) {
                        var r = s.data;
                        p.instaRequestSuccess = true;
                        a.each(r, function(t, v) {
                            var u = new h({
                                id: "",
                                wallname: "instagram",
                                title: (!e.isBlank(v.caption)) ? v.caption.text : "",
                                image: (!e.isBlank(v.images) && !e.isBlank(v.images.standard_resolution)) ? v.images.standard_resolution.url : "",
                                description: "",
                                post_url: (!e.isBlank(v.link)) ? unescape(v.link) : "",
                                likecount: (!e.isBlank(v.likes)) ? v.likes.count : 0,
                                commentcount: 0,
                                posteddate: (!e.isBlank(v.created_time)) ? parseInt(v.created_time * 1000, 10) : c,
                                page_url: p.options.InstagramPageURL,
                                isprime: false,
                                istext: false,
                                fbPoints: p.options.FacebookPoints,
                                twitterPoints: p.options.TwitterPoints,
                                linkedInPoints: p.options.LinkedInPoints,
                                retrydelay: p.options.retrydelay,
                                isPointDisplay: p.options.isPointDisplay
                            });
                            p.socialCollection.add(u);
                            p.$el.find("#socialFilters").removeClass("hide");
                            p.$el.find(".filter[rel=filterAll]").removeClass("hide");
                            p.$el.find(".filter[rel=filterInstagram]").removeClass("hide")
                        });
                        p.shufflePosts();
                        p.removeLoadingSpinner()
                    }
                })
            }
            if (!e.isBlank(p.options.GooglePlusPageID)) {
                var o = "GooglePlusPageID=" + p.options.GooglePlusPageID;
                p.gplusdata = new f.Collection([], {
                    host: "websiteoutput",
                    method: "GetGooglePlusClientFeedResponse",
                    params: o,
                    typefields: [{
                        DataType: "BOCollection",
                        Columns: "*"
                    }]
                });
                p.gplusRequestSuccess = false;
                p.gplusdata.fetch({
                    success: function(s, t) {
                        p.gplusRequestSuccess = true;
                        var r = s.models;
                        if (s != undefined && s != null) {
                            a.each(r, function(v, w) {
                                var x = c;
                                if (p.browserType == "ie8") {
                                    x = parseInt(new Date(w.get("published")).getTime(), 10)
                                } else {
                                    x = parseInt(new Date(w.get("published")).getTime(), 10)
                                }
                                var u = new h({
                                    id: (!e.isBlank(w.get("id"))) ? w.get("id") : "",
                                    wallname: "googleplus",
                                    page_name: (!e.isBlank(w.get("actor"))) ? w.get("actor") : "",
                                    title: (!e.isBlank(w.get("title"))) ? w.get("title") : "",
                                    image: (!e.isBlank(w.get("image")) && !e.isBlank(w.get("image"))) ? w.get("image") : "",
                                    description: "",
                                    post_url: (!e.isBlank(w.get("url"))) ? unescape(w.get("url")) : "",
                                    likecount: (!e.isBlank(w.get("likecount"))) ? w.get("likecount") : 0,
                                    commentcount: (!e.isBlank(w.get("commentcount"))) ? w.get("commentcount") : 0,
                                    posteddate: (!e.isBlank(x)) ? x : c,
                                    page_url: p.options.GooglePlusPageURL,
                                    isprime: false,
                                    istext: (a.trim(w.get("image")) == "" && a.trim(w.get("title")) != ""),
                                    fbPoints: p.options.FacebookPoints,
                                    twitterPoints: p.options.TwitterPoints,
                                    linkedInPoints: p.options.LinkedInPoints,
                                    retrydelay: p.options.retrydelay,
                                    isPointDisplay: p.options.isPointDisplay
                                });
                                p.socialCollection.add(u);
                                p.$el.find("#socialFilters").removeClass("hide");
                                p.$el.find(".filter[rel=filterAll]").removeClass("hide");
                                p.$el.find(".filter[rel=filterGooglePlus]").removeClass("hide")
                            });
                            p.shufflePosts();
                            p.removeLoadingSpinner()
                        }
                    },
                    error: function(r) {}
                })
            }
            if (!e.isBlank(p.options.YouTubePageID)) {
                var o = "YouTubePageID=" + p.options.YouTubePageID;
                p.youtubedata = new f.Collection([], {
                    host: "websiteoutput",
                    method: "GetYouTubeChannelList",
                    params: o,
                    typefields: [{
                        DataType: "BOCollection",
                        Columns: "*"
                    }]
                });
                p.youtubeRequestSuccess = false;
                p.youtubedata.fetch({
                    success: function(r, s) {
                        p.youtubeRequestSuccess = true;
                        if (r.length > 0) {
                            var t = r.models;
                            a.each(t, function(u, v) {
                                var w = c;
                                if (p.browserType == "ie8") {
                                    w = parseInt(new Date(v.get("published")).getTime(), 10)
                                } else {
                                    w = parseInt(new Date(v.get("published")).getTime(), 10)
                                }
                                var x = new h({
                                    id: (!e.isBlank(v.get("id"))) ? v.get("id") : "",
                                    wallname: "youtube",
                                    title: (!e.isBlank(v.get("title"))) ? v.get("title") : "",
                                    image: (!e.isBlank(v.get("image"))) && !e.isBlank(v.get("image")) ? v.get("image") : "",
                                    description: (!e.isBlank(v.get("description"))) ? v.get("description") : "",
                                    post_url: "https://www.youtube.com/watch?v=" + v.get("id"),
                                    likecount: (!e.isBlank(v.get("likecount"))) ? v.get("likecount") : 0,
                                    commentcount: (!e.isBlank(v.get("commentcount"))) ? v.get("commentcount") : 0,
                                    posteddate: (!e.isBlank(w)) ? w : c,
                                    page_url: p.options.YouTubePageURL,
                                    isprime: false,
                                    istext: false,
                                    fbPoints: p.options.FacebookPoints,
                                    twitterPoints: p.options.TwitterPoints,
                                    linkedInPoints: p.options.LinkedInPoints,
                                    retrydelay: p.options.retrydelay,
                                    isPointDisplay: p.options.isPointDisplay
                                });
                                p.socialCollection.add(x);
                                p.$el.find("#socialFilters").removeClass("hide");
                                p.$el.find(".filter[rel=filterAll]").removeClass("hide");
                                p.$el.find(".filter[rel=filterYoutube]").removeClass("hide")
                            });
                            p.shufflePosts();
                            p.removeLoadingSpinner()
                        }
                    },
                    error: function(r) {}
                })
            }
            if (!e.isBlank(p.options.TwitterPageID)) {
                var o = "TwitterPageID=" + p.options.TwitterPageID;
                p.twitterdata = new f.Collection([], {
                    host: "websiteoutput",
                    method: "GetTwitterClientFeedResponse",
                    params: o,
                    typefields: [{
                        DataType: "BOCollection",
                        Columns: "*"
                    }]
                });
                p.twitterRequestSuccess = false;
                p.twitterdata.fetch({
                    success: function(s, t) {
                        p.twitterRequestSuccess = true;
                        if (t.length > 1) {
                            var r = a.parseJSON(t[1]);
                            a.each(r, function(v, w) {
                                if (v < 10) {
                                    var x = c;
                                    x = w.created_at.replace("+0000", "UTC");
                                    x = parseInt(new Date(x).getTime(), 10);
                                    var u = "";
                                    if (w.hasOwnProperty("extended_entities")) {
                                        u = w.extended_entities.media[0].media_url_https
                                    }
                                    var y = new h({
                                        id: (!e.isBlank(w.id_str)) ? w.id_str : "",
                                        wallname: "twitter",
                                        page_name: (!e.isBlank(w.user.name)) ? w.user.name : "",
                                        title: (!e.isBlank(w.text)) ? w.text : "",
                                        image: (!e.isBlank(u)) ? u : "",
                                        description: (!e.isBlank(w.text)) ? w.text : "",
                                        post_url: "https://twitter.com/" + p.options.TwitterPageID + "/status/" + w.id_str,
                                        likecount: (!e.isBlank(w.favorite_count)) ? w.favorite_count : 0,
                                        commentcount: (!e.isBlank(w.retweet_count)) ? w.retweet_count : 0,
                                        posteddate: (!e.isBlank(x)) ? x : "",
                                        page_url: p.options.TwitterPageURL,
                                        isprime: false,
                                        istext: (a.trim(u) == "" && a.trim(w.text) != ""),
                                        fbPoints: p.options.FacebookPoints,
                                        twitterPoints: p.options.TwitterPoints,
                                        linkedInPoints: p.options.LinkedInPoints,
                                        retrydelay: p.options.retrydelay,
                                        isPointDisplay: p.options.isPointDisplay
                                    });
                                    p.socialCollection.add(y);
                                    p.$el.find("#socialFilters").removeClass("hide");
                                    p.$el.find(".filter[rel=filterAll]").removeClass("hide");
                                    p.$el.find(".filter[rel=filterTwitter]").removeClass("hide")
                                }
                            });
                            p.shufflePosts();
                            p.removeLoadingSpinner()
                        }
                    },
                    error: function(r) {}
                })
            }
        },
        updateFBPhotos: function() {
            var o = this;
            o.$el.find(".facebook").each(function(r) {
                var p = a(this).find(".itemImage:first");
                var s = a(p).attr("url");
                var u = s.split("_");
                var q = "https://graph.facebook.com/" + u[1] + "?access_token=357097134310266|p9DAlPDufisPR-5o5KT1dtkhf44";
                var t = a.ajax({
                    type: "GET",
                    url: q,
                    dataType: "jsonp",
                    success: function(v) {
                        if (v.images != null && v.images.length > 0) {
                            p.css("background-image", "url(" + v.images[0].source + ")")
                        }
                    }
                })
            })
        },
        removeLoadingSpinner: function() {
            var o = this;
            if (o.fbRequestSuccess && o.pinRequestSuccess && o.instaRequestSuccess && o.gplusRequestSuccess && o.youtubeRequestSuccess && o.twitterRequestSuccess) {
                o.$el.find("#social-spinner").ActivityIndicator("destroy");
                o.$el.find("#social-spinner").hide()
            }
        },
        shufflePosts: function() {
            var p = this;
            p.socialOrderedCollection.reset();
            if (p.socialCollection.length < 5) {
                if (p.socialCollection.length == 1) {
                    p.socialCollection.models[0].set("isprime", false)
                } else {
                    if (p.socialCollection.length == 2) {
                        p.socialCollection.models[0].set("isprime", true);
                        p.socialCollection.models[1].set("isprime", true)
                    } else {
                        if (p.socialCollection.length == 3) {
                            p.socialCollection.models[0].set("isprime", false);
                            p.socialCollection.models[1].set("isprime", false);
                            p.socialCollection.models[2].set("isprime", true)
                        } else {
                            if (p.socialCollection.length == 4) {
                                p.socialCollection.models[0].set("isprime", true);
                                p.socialCollection.models[1].set("isprime", false);
                                p.socialCollection.models[2].set("isprime", false);
                                p.socialCollection.models[3].set("isprime", true)
                            }
                        }
                    }
                }
            } else {
                var o = 1;
                p.socialCollection.each(function(s, q, r) {
                    if ((q + 1) != (5 * (o - 1)) + (o - 1)) {
                        p.socialOrderedCollection.add(s)
                    }
                    s.set("isprime", false);
                    if ((q + 1) == ((5 * o) + (o - 1))) {
                        o++;
                        if (p.socialOrderedCollection.length > 0) {
                            p.socialOrderedCollection.models[0].set("isprime", true)
                        }
                        p.socialOrderedCollection.reset()
                    }
                })
            }
            p.socialCollection.each(function(s, q, r) {
                if (!s.get("rendered")) {
                    var t = p.socialCollection.where({
                        title: s.get("title")
                    });
                    if (t.length > 1 && !e.isBlank(a.trim(s.get("title")))) {
                        t[t.length - 1].set("isDuplicate", false);
                        t.splice(t.length - 1, 1);
                        a.each(t, function(v, w) {
                            w.set("isDuplicate", true)
                        })
                    }
                    var u = new l({
                        model: s,
                        browserType: p.browserType
                    });
                    s.set("rendered", true);
                    if (q > 0) {
                        a(u.render().$el).insertAfter(p.$el.find(".item")[q - 1])
                    } else {
                        p.$el.find(".isotope").prepend(u.render().$el)
                    }
                } else {
                    if (s.get("isprime")) {
                        a(p.$el.find(".item")[q]).removeClass("regular").toggleClass("featured", true);
                        if (s.get("title").length > 90 && !(a(p.$el.find(".item")[q]).hasClass("text"))) {
                            a(p.$el.find(".item")[q]).find(".postWrapper .postTitle").text(s.get("title").substring(0, 87) + "...")
                        }
                    } else {
                        a(p.$el.find(".item")[q]).removeClass("featured").toggleClass("regular", true);
                        if (s.get("title").length > 60 && !(a(p.$el.find(".item")[q]).hasClass("text"))) {
                            a(p.$el.find(".item")[q]).find(".postWrapper .postTitle").text(s.get("title").substring(0, 57) + "...")
                        }
                    }
                }
            });
            require(["packery"], function() {
                if (p.iso != null) {
                    p.iso.destroy()
                }
                if ((/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()))) {
                    p.isotopeOptions = {
                        itemSelector: ".item",
                        columnWidth: ".regular",
                        transitionDuration: "0s",
                        layoutMode: "packery",
                        packery: {
                            isFitWidth: true,
                            gutter: 10
                        }
                    }
                } else {
                    p.isotopeOptions = {
                        itemSelector: ".item",
                        columnWidth: ".regular",
                        layoutMode: "packery",
                        packery: {
                            isFitWidth: true,
                            gutter: 10
                        }
                    }
                }
                p.iso = new d(".isotope", p.isotopeOptions)
            })
        },
        filterFeeds: function(o) {
            var r = this;
            var p = a(o.currentTarget);
            var s = a(p).attr("rel");
            var q = a(r.$el.find(".isotope"));
            a(r.$el.find(".theFilters .active")).removeClass("active");
            switch (s) {
                case "filterFacebook":
                    r.iso.arrange({
                        filter: function(t) {
                            return a(t).hasClass("facebook")
                        }
                    });
                    a(r.$el.find(".filterFacebook")).addClass("active");
                    break;
                case "filterInstagram":
                    r.iso.arrange({
                        filter: function(t) {
                            return a(t).hasClass("instagram")
                        }
                    });
                    a(r.$el.find(".filterInstagram")).addClass("active");
                    break;
                case "filterPinterest":
                    r.iso.arrange({
                        filter: function(t) {
                            return a(t).hasClass("pinterest")
                        }
                    });
                    a(r.$el.find(".filterPinterest")).addClass("active");
                    break;
                case "filterGooglePlus":
                    r.iso.arrange({
                        filter: function(t) {
                            return a(t).hasClass("googleplus")
                        }
                    });
                    a(r.$el.find(".filterGooglePlus")).addClass("active");
                    break;
                case "filterYoutube":
                    r.iso.arrange({
                        filter: function(t) {
                            return a(t).hasClass("youtube")
                        }
                    });
                    a(r.$el.find(".filterYoutube")).addClass("active");
                    break;
                case "filterTwitter":
                    r.iso.arrange({
                        filter: function(t) {
                            return a(t).hasClass("twitter")
                        }
                    });
                    a(r.$el.find(".filterTwitter")).addClass("active");
                    break;
                case "filterAll":
                    r.iso.arrange({
                        filter: function(t) {
                            return true
                        }
                    });
                    a(r.$el.find(".filterAll")).addClass("active");
                    break;
                default:
                    r.iso.arrange({
                        filter: function(t) {
                            return true
                        }
                    });
                    break
            }
        },
        destroy: function() {
            var o = this
        }
    });
    var l = b.View.extend({
        tagName: "div",
        className: "item",
        template: n,
        defaultOptions: {
            model: null,
            SitePageModuleID: null,
            FacebookPageID: "",
            PinterestPageID: "",
            InstagramPageID: "",
            InstagramClientID: "",
            browserType: ""
        },
        events: {
            "mouseenter .itemImageContainer": "imgMouseOver",
            "mouseleave .itemImageContainer": "imgMouseOut",
            "mouseenter .share": "shareMouseOver",
            "mouseleave .share": "shareMouseOut",
            "click #shareFb": "shareFb",
            "click #shareTwitter": "shareTwitter",
            "click #shareLinkedIn": "shareLinkedIn"
        },
        initialize: function() {
            var o = this;
            o.options = _.extend({}, o.defaultOptions, o.options);
            o.model.on("change", this.reRender, this)
        },
        render: function() {
            var p = this;
            var q = p.model.toJSON();
            var o = _.template(p.template, q);
            p.$el.html(o).addClass(q.wallname);
            if (q.isprime) {
                p.$el.html(o).addClass("featured")
            } else {
                p.$el.html(o).addClass("regular")
            }
            if (q.istext) {
                p.$el.html(o).addClass("text")
            } else {
                if (a.trim(q.title) == "") {
                    p.$el.find(".postWrapper").hide()
                } else {
                    if (p.$el.hasClass("regular")) {
                        if (q.title.length > 60) {
                            p.$el.find(".postWrapper .postTitle").text(q.title.substring(0, 57) + "...")
                        }
                    } else {
                        if (p.model.get("title").length > 90) {
                            p.$el.find(".postWrapper .postTitle").text(q.title.substring(0, 87) + "...")
                        }
                    }
                }
            }
            if (p.model.get("isDuplicate")) {
                p.$el.toggleClass("hide", true)
            } else {
                p.$el.toggleClass("hide", false)
            }
            p.browserSpecific();
            return p
        },
        browserSpecific: function() {
            var o = this;
            if (o.options.browserType == "ie9") {
                o.$el.find(".shareIconsContainer").removeClass("visibilityNone")
            }
        },
        reRender: function() {
            var o = this;
            if (o.model.get("isDuplicate")) {
                o.$el.toggleClass("hide", true)
            } else {
                o.$el.toggleClass("hide", false)
            }
        },
        imgMouseOver: function(o) {
            var p = this;
            o.stopImmediatePropagation();
            p.$el.find(".shareIconsContainer").css("display", "block");
            if (p.options.browserType == "ie9" || p.options.browserType == "ie8") {
                p.$el.find(".shareIconsContainer").show();
                p.$el.find(".postWrapper").hide()
            } else {
                p.$el.find(".postWrapper").addClass("fadeOutDown");
                p.$el.find(".postWrapper").removeClass("fadeInUp");
                p.$el.find(".shareIconsContainer").addClass("fadeInUp");
                p.$el.find(".shareIconsContainer").removeClass("fadeOutDown")
            }
        },
        imgMouseOut: function(o) {
            var p = this;
            o.stopImmediatePropagation();
            if (p.options.browserType == "ie9" || p.options.browserType == "ie8") {
                p.$el.find(".shareIconsContainer").hide();
                p.$el.find(".postWrapper").show()
            } else {
                p.$el.find(".postWrapper").removeClass("fadeOutDown");
                p.$el.find(".postWrapper").addClass("fadeInUp");
                p.$el.find(".shareIconsContainer").removeClass("fadeInUp");
                p.$el.find(".shareIconsContainer").addClass("fadeOutDown")
            }
        },
        shareMouseOver: function(o) {},
        shareMouseOut: function(o) {},
        shareFb: function(o) {
            var p = this;
            var q = a(o.currentTarget).closest(".item")[0];
            var s = p.model.get("title");
            s = s.replace(/[^a-zA-Z 0-9]+/g, "");
            var r = a(q).find("a.viewPostText")[0].href;
            window.open("http://www.facebook.com/sharer.php?u=" + encodeURIComponent(r) + "&t=" + encodeURIComponent(s), "sharer", "toolbar=0,status=0,width=450,height=350");
            e.Tracker("17306", a("[id$=SitePageID]").val(), "", "1162", true);
            var t = (o.currentTarget) ? o.currentTarget : o.srcElement;
            console.log(a(t));
            require(["headerv1"], function(u) {
                u.triggerLoyalityRewardPoints("[id$=spnFbPoints]", t, 17306)
            })
        },
        shareTwitter: function(p) {
            var q = this;
            var r = a(p.currentTarget).closest(".item")[0];
            var t = q.model.get("title");
            t = t.replace(/[^a-zA-Z 0-9]+/g, "");
            var o = a(r).find("#shareTwitter").attr("data-related");
            var s = a(r).find("a.viewPostText")[0].href;
            this.getShortenURL(s, function(w) {
                var v = "https://twitter.com/intent/tweet?related=" + o + "&text=" + decodeURIComponent(t) + " " + decodeURIComponent(w);
                if ((t.length + w.length + 5) > 140) {
                    t = t.substring(0, 115);
                    v = "https://twitter.com/intent/tweet?related=" + o + "&text=" + decodeURIComponent(t) + " " + decodeURIComponent(w)
                }
                window.open(v, "", "toolbar=0,status=0,width=450,height=350");
                e.Tracker("17307", a("[id$=SitePageID]").val(), "", "1162", true);
                var u = (p.currentTarget) ? p.currentTarget : p.srcElement;
                console.log(a(u));
                require(["headerv1"], function(x) {
                    x.triggerLoyalityRewardPoints("[id$=spnTwitterPoints]", u, 17307)
                })
            })
        },
        getShortenURL: function(p, o) {
            var q = this;
            e.URL.Shortener(p, function(u, s, r, t) {
                o(u)
            })
        },
        shareLinkedIn: function(o) {
            var p = this;
            var r = a(o.currentTarget).closest(".item")[0];
            var t = p.model.get("title");
            t = t.replace(/[^a-zA-Z 0-9]+/g, "");
            var s = a(r).find("a.viewPostText")[0].href;
            var q = "http://www.linkedin.com/shareArticle?mini=true&url=" + decodeURIComponent(s) + "&summary=" + decodeURIComponent(t) + "&source=" + decodeURIComponent(s);
            window.open(q, "linkedin", "toolbar=0,status=0,width=450,height=550");
            e.Tracker("17311", a("[id$=SitePageID]").val(), "", "1162", true);
            var u = (o.currentTarget) ? o.currentTarget : o.srcElement;
            console.log(a(u));
            require(["headerv1"], function(v) {
                v.triggerLoyalityRewardPoints("[id$=spnLinkedInPoints]", u, 17311)
            })
        },
        shareGooglePlus: function(o) {
            var p = this;
            var r = a(o.currentTarget).closest(".item")[0];
            var t = p.model.get("title");
            t = t.replace(/[^a-zA-Z 0-9]+/g, "");
            var s = a(r).find("a.viewPostText")[0].href;
            var q = "http://plus.google.com/share?url=" + decodeURIComponent(s)
        },
        stopPropagation: function(o) {
            var p = this;
            o.stopImmediatePropagation()
        },
        popupWindow: function(s, q, t, o) {
            var p = (screen.width / 2) - (400 / 2);
            var r = (screen.height / 2) - (400 / 2);
            window.open(s, q, "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=400px, height=400px, top=" + r + ", left=" + p)
        },
        destroy: function() {
            var o = this
        }
    });
    return m
});
