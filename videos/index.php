<?php
    $page_title="Videos - k02.fit";
    $page_description="For more than 7 years Fit4All Fitness Centers have taken pride in helping our people push for success with all of their goals by bringing excellent training, encouragement and high energy.";
    $page_keywords="training, trainer, diet, girl, weight, personal, shape, pumping, stamina, up, strength, background, light, health, biceps, hands,heavy, bodybuilding, sportswoman, muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                        <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem" class="active">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem" class="active">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_divModParallax" class="mod-parallax mod-parallax-dd75feaa-9491-4f7b-b0ff-7f77d715f7f8 parallax-layout-2 padBg" data-speed="10" sitepagemoduleid="dd75feaa-9491-4f7b-b0ff-7f77d715f7f8" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" style="max-height: 500px;">
            <img id="ctl00_bgParallaxImg" class="scroll-Parallax-image" sitepagemoduleid="dd75feaa-9491-4f7b-b0ff-7f77d715f7f8" contentitemid="a7b5e66d-61c1-40ad-bab7-5842820ac1b0" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            <div>
                <img id="ctl00_invisibleParallaxImg" class="hidden-Parallax-image" sitepagemoduleid="dd75feaa-9491-4f7b-b0ff-7f77d715f7f8" contentitemid="a7b5e66d-61c1-40ad-bab7-5842820ac1b0" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            </div>
            <div class="parallax-text-wrap">
                <div class="parallax-text-table">
                    <div id="ctl00_divHeader" class="parallaxtext">
                        <h3 id="ctl00_litParallaxTitle">Videos</h3>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["jquery", "imageparallaxv1"], function($, ImageParallaxV1) {
            ImageParallaxV1.loadModule("dd75feaa-9491-4f7b-b0ff-7f77d715f7f8");
        });
        </script>
    </div>
    <div class='centerContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span6'>
                        <div class="mod-embed embed-layout-1">
                            <div class="mod-embed-iframe">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/R_Fx6JZ1NS0?uc=_modulesembedv1ucEmbedDefault.ascx&amp;msid=00000000-0000-0000-0000-000000000000&amp;spmid=80f2efc8-2c2d-44f2-b2e1-6d7c721664d5&amp;pgid=a6a84f0f-f4e3-4e05-8a83-e12041273334&amp;siteid=ac9ddd90-0beb-4fbe-b677-ce09b3996285&amp;locale=en&amp;id=a6a84f0f-f4e3-4e05-8a83-e12041273334&amp;" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                    <div class='span6'>
                        <div class="mod-embed embed-layout-1">
                            <div class="mod-embed-iframe">
                                <iframe width="300" height="300" src="https://www.youtube.com/embed/aUKmkU1CusU?uc=_modulesembedv1ucEmbedDefault.ascx&amp;msid=00000000-0000-0000-0000-000000000000&amp;spmid=450eee06-e53e-4c19-b338-e51ef46fe18f&amp;pgid=a6a84f0f-f4e3-4e05-8a83-e12041273334&amp;siteid=ac9ddd90-0beb-4fbe-b677-ce09b3996285&amp;locale=en&amp;id=a6a84f0f-f4e3-4e05-8a83-e12041273334&amp;" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class='span6'>
                        <div id="ctl00_divModVideo" class="mod_video video-layout-1">
                            <div id="ctl00_videoWrapper" class="video-player">
                                <video id="ctl00_videoMyVideo_fc68298c-996e-4439-a438-13ff2c63865f" width="100%" height="100%" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/6c16c066-58a4-4703-8eeb-e995e2fbf4db.mp4" poster="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/67f217b8-c40d-43fc-b402-99109d069a18_h.jpg" type="video/mp4" controls="controls" preload="none" style="height:100%; width:100%;"></video>
                            </div>
                        </div>
                        <div id="ctl00_bwtest_fc68298c-996e-4439-a438-13ff2c63865f"></div>
                        <script type="text/javascript">
                        require(["videov1"], function(VideoV1) {
                            var VideoData = {
                                elPlayerID: "#ctl00_videoMyVideo_fc68298c-996e-4439-a438-13ff2c63865f",
                                elBwtestID: "#ctl00_bwtest_fc68298c-996e-4439-a438-13ff2c63865f",
                                PlayerOptions: {
                                    "defaultVideoWidth": 480,
                                    "defaultVideoHeight": 270,
                                    "videoWidth": -1,
                                    "videoHeight": -1,
                                    "audioWidth": 400,
                                    "audioHeight": 30,
                                    "framesPerSecond": 25,
                                    "startVolume": 0.8,
                                    "loop": false,
                                    "enableAutosize": true,
                                    "alwaysShowControls": false,
                                    "iPadUseNativeControls": false,
                                    "iPhoneUseNativeControls": false,
                                    "AndroidUseNativeControls": false,
                                    "alwaysShowHours": false,
                                    "showTimecodeFrameCount": false,
                                    "enableKeyboard": true,
                                    "pauseOtherPlayers": false,
                                    "autoPlay": false,
                                    "KeyActions": [""],
                                    "features": ["playpause", "progress", "current", "duration", "tracks", "volume", "fullscreen"],
                                    "videoUrl_H": "/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/6c16c066-58a4-4703-8eeb-e995e2fbf4db.mp4",
                                    "videoUrl_L": "",
                                    "plugins": ["flash"],
                                    "pluginPath": "/assets-cloudfront/moprosuite/v10/uikit/_static/_player/",
                                    "flashName": "flashmediaelement.swf"
                                }
                            };
                            var objVideoV1 = new VideoV1(VideoData).loadModule();
                        });
                        </script>
                    </div>
                    <div class='span6'>
                        <div class="mod-embed embed-layout-1">
                            <div class="mod-embed-iframe">
                                <iframe width="300" height="300" src="https://www.youtube.com/embed/fT5ccy2sBi0?uc=_modulesembedv1ucEmbedDefault.ascx&amp;msid=00000000-0000-0000-0000-000000000000&amp;spmid=15f5fff8-ed10-4c5b-bd7c-413ac6c384e2&amp;pgid=a6a84f0f-f4e3-4e05-8a83-e12041273334&amp;siteid=ac9ddd90-0beb-4fbe-b677-ce09b3996285&amp;locale=en&amp;id=a6a84f0f-f4e3-4e05-8a83-e12041273334&amp;" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>