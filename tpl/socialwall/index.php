
<!--wrapping image in container to hide overflow-->
<div class="preLoad"></div>
<div class="itemImageContainer">

    <!--social image-->

    <div class="itemImage" style='background: url("<%=image%>") center center' url="<%=image%>"></div>

    <!--post title, description & text-->

    <div class="postWrapper animated">
        <h1 class="postBiz"><%=page_name %></h1>
        <hr class="textBreak" />
        <h1 class="postTitle"><%=title %></h1>
        <p class="postDesc"></p>
    </div>

    <!--hover state, share icons, view post link, controlled in javascript-->

    <div class="shareIconsContainer animated visibilityNone" style="display:none;">
        <div class="shareIcons animated text-centerNew">
            <h1 class="viewPostText theShareText">Share</h1>
            <div class="shareTransition share animated sFb" id="shareFb" ispointdisplay="<%=isPointDisplay %>" retrydelay="<%=retrydelay %>" data-tip="<%=fbPoints %>"></div>
            <hr class="socialLine" width="1">
            <div class="shareTransition share animated sTwitter" href="https://twitter.com/share" id="shareTwitter" ispointdisplay="<%=isPointDisplay %>" retrydelay="<%=retrydelay %>" data-tip="<%=twitterPoints %>"></div>
            <hr class="socialLine" width="1">
            <div class="shareTransition share animated sLinkedIn" id="shareLinkedIn" ispointdisplay="<%=isPointDisplay %>" retrydelay="<%=retrydelay %>" data-tip="<%=linkedInPoints %>"></div>
            <div class="viewPost animated">
                <a class="viewPostText" href="<%=post_url %>" target="_blank">View Post</a>
            </div>
        </div>
    </div>
</div>


<!--social bar at bottom, styling controlled by parent classes-->

<div class="bar">

    <!--timestamp, counts/icons for likes, pins, comments, etc-->

    <div class="barTextContainer">
        <div class="action">
            <div class="actionIcon"></div>
            <p class="actionCount"><%=likecount %></p>
        </div>
        <img class="timestamp clock" src="assets/img/clock.png">
        <p class="timestamp clockText"><%=postedtimediff %></p>
        <a class="viewPostMobile timestamp" href="<%=post_url %>" target="_blank">View Post</a>
    </div>

    <!--social network icon, controlled by parent classes-->

    <a href="<%=page_url %>" target="_blank"><div class="theNetwork"></div></a>
</div>