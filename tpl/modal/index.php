<!-- BEGIN TPL/MODAL -->
<div id="modal" style="display: block;">
    <div class="overlay"></div>


    <div class="popup <%= className %>">

        <div class="popup-block small-popup visible" style="max-width: <%= width %>px;">
            <div class="popup-inner-block">
                <a class="popup-close" href="javascript: void(0);" data-autoclose="true">Close</a>
                <div class="popup-title-block">



                </div>
                <div class="popup-container">


                </div>
            </div>

        </div>
    </div>
</div>


<!-- END TPL/MODAL -->