<?php
    $page_title="Burn 800 calories in 3 minutes! - Blog - k02.fit";
    $page_description="Over the past 15 years, Michael Delguyd has built a reputation for being a leader and a pioneer in the personal training and fitness industry. Michael began his journey with an industry giant, Bally's Total Fitness. His scorching and limitless desire to impact the fitness industry along with his aristocratic quest for knowledge helped him breeze pa";
    $page_keywords="fit, fitness, gym, workout, crossfit, strong, training, trainer, diet, girl, weight, personal, shape, pumping, stamina, up, strength, background, light, health, biceps, hands,heavy, bodybuilding, sportswoman, muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic, cross-fit,sportswear, sport";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                         <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem" class="active">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem" class="active">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_divModParallax" class="mod-parallax mod-parallax-84acf3af-9023-4856-af2b-a417f75ed5e2 parallax-layout-2 padBg" data-speed="10" sitepagemoduleid="84acf3af-9023-4856-af2b-a417f75ed5e2" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" style="max-height: 500px;">
            <img id="ctl00_bgParallaxImg" class="scroll-Parallax-image" sitepagemoduleid="84acf3af-9023-4856-af2b-a417f75ed5e2" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            <div>
                <img id="ctl00_invisibleParallaxImg" class="hidden-Parallax-image" sitepagemoduleid="84acf3af-9023-4856-af2b-a417f75ed5e2" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            </div>
            <div class="parallax-text-wrap">
                <div class="parallax-text-table">
                    <div id="ctl00_divHeader" class="parallaxtext">
                        <h3 id="ctl00_litParallaxTitle">Blog</h3>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["jquery", "imageparallaxv1"], function($, ImageParallaxV1) {
            ImageParallaxV1.loadModule("84acf3af-9023-4856-af2b-a417f75ed5e2");
        });
        </script>
    </div>
    <div class="centerContainer">
<div class="mainContainer">
<div class="container">
<div class="row">
<div class="span12">


<div class="blog-post clearfix">

    <div class="post-date">
        <span class="post-month">Jun  </span> <span class="post-day">30  </span>
    </div>


    <div class="mod-header">

        <h3 id="ctl00_h3HeaderPost">Burn 800 calories in 3 minutes!</h3>

    </div>


    <div id="ctl00_divTags" class="post-tags">

    <a id="ctl00_rptTags_ctl00_lnkTags">health\,</a>
    <a id="ctl00_rptTags_ctl01_lnkTags">streetsboro,</a>
    <a id="ctl00_rptTags_ctl02_lnkTags">wellness,</a>
    <a id="ctl00_rptTags_ctl03_lnkTags">burn,</a>
    <a id="ctl00_rptTags_ctl04_lnkTags">cryotherapy,</a>
    <a id="ctl00_rptTags_ctl05_lnkTags">fitness,</a>
    <a id="ctl00_rptTags_ctl06_lnkTags">personal,</a>
    <a id="ctl00_rptTags_ctl07_lnkTags">trainer,</a>
    <a id="ctl00_rptTags_ctl08_lnkTags">ko2,</a>
    <a id="ctl00_rptTags_ctl09_lnkTags">calories,</a>
    <a id="ctl00_rptTags_ctl10_lnkTags">ohio,</a>
    <a id="ctl00_rptTags_ctl11_lnkTags">cryo,</a>
    <a id="ctl00_rptTags_ctl12_lnkTags">hudson</a>
    </div>


    <div id="ctl00_divPostDescription" class="post-description clearfix" data-contentitemid="869d5dac-bfd4-4ea5-b123-203d43f5e746">
<div class="post-video">
<h3>Cryotherapy: New Health Trend?</h3>
<div class="mod-embed">
<div class="mod-embed-iframe"><iframe class="embedly-embed" src="http://cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2FD1FPxhs8_H0%3Fwmode%3Dopaque%26feature%3Doembed&amp;wmode=opaque&amp;url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DD1FPxhs8_H0&amp;image=https%3A%2F%2Fi.ytimg.com%2Fvi%2FD1FPxhs8_H0%2Fhqdefault.jpg&amp;key=384aa6bed47d488d8b1c36b675b38322&amp;type=text%2Fhtml&amp;schema=youtube" frameborder="0" scrolling="no" width="600" height="338"></iframe></div>
</div>
<p> </p>
</div>
<p><strong style="color: #cccccc;"> </strong></p></div>


    <div class="post-info"><p>Posted on 06/30/2015 at 06:38:00 AM</p></div>
    <div class="share">
        <ul>
<li id="ctl00_liFacebook" class="share-facebook rewards">
    <a id="ctl00_lnkBlogFacebook">
        <div class="socialMediaSharingCls facebook hide"></div>
        <div id="ctl00_like" class="fb-like btn-facebook-like" data-send="false" data-layout="button_count" data-width="90" data-show-faces="false" data-href="<?=$_SERVER['HTTP_HOST'];?>/blog/post/burn-800-calories-in-3-minutes" data-title="Burn 800 calories in 3 minutes!"></div>

    </a>
</li>
<li id="ctl00_liTwitter" class="share-twitter">
    <a id="ctl00_lnkBlogTwitter">
        <div id="ctl00_aTweetButton" href="https://twitter.com/share" data-hashtags="health\,streetsboro,wellness,burn,cryotherapy,fitness,personal,trainer,ko2,calories,ohio,cryo,hudson" data-url="<?=$_SERVER['HTTP_HOST'];?>/blog/post/burn-800-calories-in-3-minutes" data-text="Burn 800 calories in 3 minutes!">
  <img src="/assets-mopro/default-theme/blog/images/social-icon-tweet.png" alt="Share on Twitter">
        </div>

    </a>
</li>
<li id="ctl00_liLinkedIn" class="share-linkedin">
    <span id="ctl00_lnkBlogLinkedIn">
        <script type="IN/Share" data-width="100" data-url="<?=$_SERVER['HTTP_HOST'];?>/blog/post/burn-800-calories-in-3-minutes"></script>
    </span>

</li>
<li class="share-pintrest">
    <a id="hlPintrest">
        <div id="ctl00_btnPinIt" class="pin-it" data-url="<?=$_SERVER['HTTP_HOST'];?>/blog/post/burn-800-calories-in-3-minutes">
 <img src="/assets-mopro/default-theme/blog/images/social-icon-pinit.png" alt="Share on Pinterest">
        </div>

     </a>
</li>
<li class="share-googleplus">
    <a id="hlGplus">
        <div id="ctl00_btngplus" data-href="<?=$_SERVER['HTTP_HOST'];?>/blog/post/burn-800-calories-in-3-minutes">
<img src="/assets-mopro/default-theme/blog/images/social-icon-googleplus.png" alt="Share on Google+">
        </div>

    </a>
</li>
<li style="display:none;" class="share-email"><a id="ctl00_hlEmailIt">Email It</a></li>
        </ul>
    </div>


</div>

<script type="text/javascript">
    require(["blogv1"], function ( blogv1) {
        //TODO
        blog = blogv1;
        blog.loadModule('1');
    });
</script></div>
</div>
</div></div></div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>