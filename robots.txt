User-agent: *
Disallow: /endpoints/
Disallow: /assets-cloudfront/
Disallow: /assets-mopro/
Disallow: /assets-typekit/
Disallow: /tpl/
Disallow: /inc/