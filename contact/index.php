<?php
    $page_title="Contact - k02.fit";
    $page_description="Michael began his journey with an industry giant, Bally's Total Fitness. His scorching and limitless desire to impact the fitness industry along with his aristocratic quest for knowledge helped him breeze past his colleagues and eventually earn the position of area manager at the young age of 22.";
    $page_keywords="pumping, stamina, up, strength, background, light, health, biceps, hands,heavy, bodybuilding, sportswoman, muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic, cross-fit,sportswear, sport";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                         <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem" class="active">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem" class="active">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_divModParallax" class="mod-parallax mod-parallax-5c7781a7-156d-433b-b77a-bcf1c9297c91 parallax-layout-2 padBg" data-speed="10" sitepagemoduleid="5c7781a7-156d-433b-b77a-bcf1c9297c91" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" style="max-height: 500px;">
            <img id="ctl00_bgParallaxImg" class="scroll-Parallax-image" sitepagemoduleid="5c7781a7-156d-433b-b77a-bcf1c9297c91" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            <div>
                <img id="ctl00_invisibleParallaxImg" class="hidden-Parallax-image" sitepagemoduleid="5c7781a7-156d-433b-b77a-bcf1c9297c91" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            </div>
            <div class="parallax-text-wrap">
                <div class="parallax-text-table">
                    <div id="ctl00_divHeader" class="parallaxtext">
                        <h3 id="ctl00_litParallaxTitle">Contact</h3>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["jquery", "imageparallaxv1"], function($, ImageParallaxV1) {
            ImageParallaxV1.loadModule("5c7781a7-156d-433b-b77a-bcf1c9297c91");
        });
        </script>
    </div>
    <div class='centerContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span12'>
                        <div id="ctl00_paddingWrapper" class="mod-padding" style="height: 35px;"></div>
                        <div id="ctl00_divModMaps" class="mod_map location-layout-2">
                            <div class="clearfix">
                                <div id="ctl00_divMap_626fd64f-c64c-46c9-aa2b-06f126ad28b0" class="map"></div>
                                <address id="ctl00_rptAddresses_ctl00_addrlocation">
                                    <h3 id="ctl00_rptAddresses_ctl00_lblCaption_0_626fd64f-c64c-46c9-aa2b-06f126ad28b0">Cleveland Knockouts</h3>
                                    <p id="ctl00_rptAddresses_ctl00_pAddressInfo">7007 Mill RD, </p>
                                    <p id="ctl00_rptAddresses_ctl00_pAddressInfoTwo"></p>
                                    <p id="ctl00_rptAddresses_ctl00_pStateZip">Brecksville, OH 44141</p>
                                    <p id="ctl00_rptAddresses_ctl00_pPhonenum">Phone. 440-488-2486</p>
                                    <div class="art-reward-points">
                                        <div class="art-reward-points"><a href="https://maps.google.com/maps?q=7007%20Mill%20RD%2C%20Brecksville%2C%20OH%2044141&amp;hl=en&amp;t=m&amp;z=16&amp;output=classic" id="ctl00_rptAddresses_ctl00_lnkGetDirection" class="btn btn-direction" target="_blank" onclick="if(typeof(MO) != &#39;undefined&#39; &amp;&amp; MO.Tracker != undefined &amp;&amp; typeof(MO.Tracker) == &#39;function&#39;){MO.Tracker(&#39;17305&#39;,&#39;ab1134dc-a672-43da-af4f-8a82051f9850&#39;,&#39;&#39;,&#39;17305&#39;,true);Header.triggerLoyalityRewardPoints(&#39;[id$=spnDirections]&#39;, this,17305);}">Get Directions</a>
                                        </div>
                                    </div>
                                </address>
                                <input name="ctl00$hdnAddress_626fd64f-c64c-46c9-aa2b-06f126ad28b0" type="hidden" id="ctl00_hdnAddress_626fd64f-c64c-46c9-aa2b-06f126ad28b0" value="[[&#39;Cleveland Knockouts7007 Mill RDBrecksville,OH-44141&#39;,&#39;7007 Mill RD Brecksville OH 44141&#39;,&#39;lblCaption_0_626fd64f-c64c-46c9-aa2b-06f126ad28b0&#39;]]" />
                                <input name="ctl00$hdnyES" type="hidden" id="ctl00_hdnyES" />
                            </div>
                        </div>
                        <script language='javascript'>
                        require(["jquery", "locationv1"], function($, locationv1) {
                            locationv1.loadModule('ctl00_divMap_626fd64f-c64c-46c9-aa2b-06f126ad28b0', 'ctl00_hdnAddress_626fd64f-c64c-46c9-aa2b-06f126ad28b0');
                        });
                        </script>
                    </div>
                </div>
                <div class='row'>
                    <div class='span8'>
                        <form id="ctl00_divModContactForm" class="mod-contact contactform-layout-4" action="/endpoints/contact/" method="POST">
                            <fieldset>
                                <div class="form-container clearfix">
                                    <div id="ctl00_parentheaderdiv" class="mod-header">
                                        <h3 id="ctl00_h3Title" class="form-heading">Take the first swing on us</h3>
                                    </div>
                                    <div class="formWrap">
                                        <div class="alert alert-success divContactFormSuccessMessage" style="display: none;">
                                            Success, your message was sent. Thanks!
                                        </div>
                                        <div class="alert alert-error divContactFormFailureMessage" style="display: none;">
                                            Sorry, we failed to send your message!
                                        </div>
                                        <div class="form-left">
                                            <div id="ctl00_divName" class="control-group divName">
                                                <div class="controls">
                                                    <label class="field">
                                                        Name&nbsp;*</label>
                                                    <input name="ctl00$txtName" type="text" id="ctl00_txtName" data-placehold="Type your name" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-parentid="#ctl00_divName" /><span class="help-inline"></span>
                                                </div>
                                            </div>
                                            <div id="ctl00_divEmail" class="control-group divEmail">
                                                <div class="controls">
                                                    <label class="field">
                                                        Email&nbsp;*</label>
                                                    <input name="ctl00$txtEmail" type="text" id="ctl00_txtEmail" data-placehold="Type your address" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-validation="email" data-parentid="#ctl00_divEmail" /><span class="help-inline"></span>
                                                </div>
                                            </div>
                                            <div id="ctl00_divSubject" class="control-group divSubject">
                                                <div class="controls">
                                                    <label class="field">
                                                        Subject&nbsp;*</label>
                                                    <input name="ctl00$txtSubject" type="text" id="ctl00_txtSubject" data-placehold="Type your subject" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-parentid="#ctl00_divSubject" />
                                                    <span class="help-inline"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-right">
                                            <div id="ctl00_divMessage" class="control-group divMessage">
                                                <div class="controls">
                                                    <label class="field">
                                                        Message&nbsp;*</label>
                                                    <textarea name="ctl00$txtMessage" id="ctl00_txtMessage" cols="20" rows="7" data-placehold="Type your message" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-parentid="#ctl00_divMessage"></textarea>
                                                    <span class="help-inline"></span>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="controls-reward-points">
                                                        <a id="ctl00_aSubmit" class="btn btn-primary" data-loading-text="Sending..." autocomplete="off" onclick="javascript:ContactForm.sendEmail(event,this,&#39;294949a7-5896-40e7-aa5e-d8611ff60e5d&#39;);">Send Message </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                        <script type="text/javascript">
                        require(["contactformv1"], function(contactformv1) {
                            ContactForm = contactformv1;
                        });
                        </script>
                    </div>
                    <div class='span4'>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-b2dd2b43-0768-4614-adc0-c24799dcd603 article-layout-1" sitepagemoduleid="b2dd2b43-0768-4614-adc0-c24799dcd603" contentitemid="efe1259a-6480-4842-8136-a1fd0992caf2">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3 class="align-center">Hours</h3>
                                    <p class="align-center">Daily: 5:30am - 8:30pm</p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "b2dd2b43-0768-4614-adc0-c24799dcd603",
                                IsPublish: 1
                            });
                        });
                        </script>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-0acc2ca3-8b06-4011-adb4-8fec2b6f8071 article-layout-2" sitepagemoduleid="0acc2ca3-8b06-4011-adb4-8fec2b6f8071" contentitemid="84ba4ff3-f633-49ea-b860-e455e5d1f489">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3 class="align-center"> </h3>
                                    <h3 class="align-center"> </h3>
                                    <h3 class="align-center">WE LOOK FORWARD TO</h3>
                                    <h3 class="align-center">HEARING FROM YOU</h3>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "0acc2ca3-8b06-4011-adb4-8fec2b6f8071",
                                IsPublish: 1
                            });
                        });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>