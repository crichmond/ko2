<?php
    $page_title="About - k02.fit";
    $page_description="Over the past 15 years, Michael Delguyd has built a reputation for being a leader and a pioneer in the personal training and fitness industry. Michael began his journey with an industry giant, Bally's Total Fitness.";
    $page_keywords="muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic, cross-fit,sportswear, sport";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                         <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem" class="active">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem" class="active">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_divModParallax" class="mod-parallax mod-parallax-84acf3af-9023-4856-af2b-a417f75ed5e2 parallax-layout-2 padBg" data-speed="10" sitepagemoduleid="84acf3af-9023-4856-af2b-a417f75ed5e2" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" style="max-height: 500px;">
            <img id="ctl00_bgParallaxImg" class="scroll-Parallax-image" sitepagemoduleid="84acf3af-9023-4856-af2b-a417f75ed5e2" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            <div>
                <img id="ctl00_invisibleParallaxImg" class="hidden-Parallax-image" sitepagemoduleid="84acf3af-9023-4856-af2b-a417f75ed5e2" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            </div>
            <div class="parallax-text-wrap">
                <div class="parallax-text-table">
                    <div id="ctl00_divHeader" class="parallaxtext">
                        <h3 id="ctl00_litParallaxTitle">About</h3>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["jquery", "imageparallaxv1"], function($, ImageParallaxV1) {
            ImageParallaxV1.loadModule("84acf3af-9023-4856-af2b-a417f75ed5e2");
        });
        </script>
    </div>
    <div class='centerContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span12'>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-2446552d-1fee-44c1-96f9-c83c78a73a3f article-layout-1" sitepagemoduleid="2446552d-1fee-44c1-96f9-c83c78a73a3f" contentitemid="33da430f-eb74-4fac-9db3-2a91a62e1c54">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_ImageWrapper" class="article-photo">
                                    <a id="ctl00_ImageLink" class="curs-default">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/0987c12f-fbd4-4e25-9eba-ade8a4cd0ba0.jpg" id="ctl00_Image" class="articleImg thumbnail" sitepagemoduleid="2446552d-1fee-44c1-96f9-c83c78a73a3f" contentitemid="33da430f-eb74-4fac-9db3-2a91a62e1c54" />
                                    </a>
                                </div>
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3>Michael Anthony Delguyd- Founder</h3>
                                    <p>Over the past 15 years, Michael Delguyd has built a reputation for being a leader and a pioneer in the personal training and fitness industry. Michael began his journey with an industry giant, Bally's Total Fitness. His scorching and limitless desire to impact the fitness industry along with his aristocratic quest for knowledge helped him breeze past his colleagues and eventually earn the position of area manager at the young age of 22. At such a young age Michael was one of the top area managers in the nation, overseeing 5 clubs, 400 employees and exceeding his quota by an astonishing 341%. After a few more years of consulting for under performing fitness clubs and turning them around, his entrepreneurial spirit kicked in and he opened his very own fitness center. For more than 7 years Fit4All Fitness Centers have taken pride in helping our people push for success with all of their goals by bringing excellent training, encouragement and high energy. Despite the constant changes and trends in the world of fitness, one part of the business has always stayed consistent--having a passion for helping people reach their maximum potential; whether it be staff getting the best results from their hard work, or the clients reaching their goals because we push them to the limit. Years of top notch leadership and business savvy, accompanying a sickening work ethic has proven one thing, Michael Delguyd is truly cut from a different cloth.</p>
                                    <h3>+1 Mindset: People. Helping. People.</h3>
                                    <p>What does the +1 Mindset mean? +1 is what happens every time you perform a kind act to make someone else's life better or a little less stressful. Carrying grocery's for a stranger if they are not able, leading someone to more knowledge or even something as small as giving someone a sincere compliment. Working to the best of your ability every day because you now it will make you better, as well as the people you position yourself around. Eventually someone else will experience what +1 feels like from being around you and share the feeling with those who are around them. The +1 mindset is contagious. +1= People. Helping. People.</p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[{&quot;href&quot;:&quot;/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/0987c12f-fbd4-4e25-9eba-ade8a4cd0ba0.jpg&quot;,&quot;title&quot;:&quot;&lt;h3>Michael Anthony Delguyd- Founder&lt;/h3>\n&lt;p>Over the past 15 years, Michael Delguyd has built a rep&quot;,&quot;thumbnail&quot;:&quot;/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/0987c12f-fbd4-4e25-9eba-ade8a4cd0ba0_m.jpg&quot;}]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "2446552d-1fee-44c1-96f9-c83c78a73a3f",
                                IsPublish: 1
                            });
                        });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>