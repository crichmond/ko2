<?php
    $page_title="Core. Tone. Box. - k02.fit";
    $page_description="Over the past 15 years, Michael Delguyd has built a reputation for being a leader and a pioneer in the personal training and fitness industry. Michael began his journey with an industry giant, Bally's Total Fitness. His scorching and limitless desire to impact the fitness industry along with his aristocratic quest for knowledge helped him breeze pa";
    $page_keywords="fit, fitness, gym, workout, crossfit, strong, training, trainer, diet, girl, weight, personal, shape, pumping, stamina, up, strength, background, light, health, biceps, hands,heavy, bodybuilding, sportswoman, muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic, cross-fit,sportswear, sport";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                         <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_divModParallax" class="mod-parallax mod-parallax-f449cd12-0c3d-4ed3-8ff1-2d596ba2a5df parallax-layout-2 padBg" data-speed="10" sitepagemoduleid="f449cd12-0c3d-4ed3-8ff1-2d596ba2a5df" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" style="max-height: 500px;">
            <img id="ctl00_bgParallaxImg" class="scroll-Parallax-image" sitepagemoduleid="f449cd12-0c3d-4ed3-8ff1-2d596ba2a5df" contentitemid="6edcdcf2-5435-49f2-8364-148bc9a36f75" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            <div>
                <img id="ctl00_invisibleParallaxImg" class="hidden-Parallax-image" sitepagemoduleid="f449cd12-0c3d-4ed3-8ff1-2d596ba2a5df" contentitemid="6edcdcf2-5435-49f2-8364-148bc9a36f75" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            </div>
            <div class="parallax-text-wrap">
                <div class="parallax-text-table">
                    <div id="ctl00_divHeader" class="parallaxtext">
                        <h3 id="ctl00_litParallaxTitle">3 Step Process</h3>
                        <h6 id="ctl00_litParallaxSubTitle">Breathe Easy, Train Hard</h6>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["jquery", "imageparallaxv1"], function($, ImageParallaxV1) {
            ImageParallaxV1.loadModule("f449cd12-0c3d-4ed3-8ff1-2d596ba2a5df");
        });
        </script>
    </div>
    <div class='centerContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span12'>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-9feb456b-2f45-4804-a26c-54bb94e23da5 article-layout-1" sitepagemoduleid="9feb456b-2f45-4804-a26c-54bb94e23da5" contentitemid="c7772d8a-63f2-4dd6-b830-f57f13785961">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3 class="align-center">1 Session = 3 Stages</h3>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "9feb456b-2f45-4804-a26c-54bb94e23da5",
                                IsPublish: 1
                            });
                        });
                        </script>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-9c99280d-3b99-4c15-a632-f1c7fa96f955 article-layout-2" sitepagemoduleid="9c99280d-3b99-4c15-a632-f1c7fa96f955" contentitemid="ccaaf368-9ace-4384-b9fe-6f0a3fbb1ec3">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_ImageWrapper" class="article-photo">
                                    <a id="ctl00_ImageLink" class="curs-default">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/98e353be-4bdf-4e92-9b24-1dafc6394e04.png" id="ctl00_Image" class="articleImg thumbnail" sitepagemoduleid="9c99280d-3b99-4c15-a632-f1c7fa96f955" contentitemid="ccaaf368-9ace-4384-b9fe-6f0a3fbb1ec3" />
                                    </a>
                                </div>
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3>Core</h3>
                                    <p>Bringing the new trend to northeast Ohio, Cleveland Knockouts will use gladiator walls for workouts to focus on our client's posture and core strength. The gladiator walls are used as a prop to help our clients balance. The gladiator walls ensure specific muscles are strengthened, and increases body alignment during exercises. High reps of small range-of-motion movements guarantee a whole body workout.</p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[{&quot;href&quot;:&quot;/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/98e353be-4bdf-4e92-9b24-1dafc6394e04.png&quot;,&quot;title&quot;:&quot;&lt;h3>Core&lt;/h3>\n&lt;p>Bringing the new trend to northeast Ohio, Cleveland Knockouts will use gladiator wa&quot;,&quot;thumbnail&quot;:&quot;/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/98e353be-4bdf-4e92-9b24-1dafc6394e04_m.png&quot;}]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "9c99280d-3b99-4c15-a632-f1c7fa96f955",
                                IsPublish: 1
                            });
                        });
                        </script>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-765f828a-513d-45d2-847b-6a09cabfc0f4 article-layout-1" sitepagemoduleid="765f828a-513d-45d2-847b-6a09cabfc0f4" contentitemid="64352d31-35b3-4c68-8e70-175fc2b8118b">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_ImageWrapper" class="article-photo">
                                    <a id="ctl00_ImageLink" class="curs-default">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/49f4546a-3f28-4db9-914f-13b8592f025d.jpg" id="ctl00_Image" class="articleImg thumbnail" sitepagemoduleid="765f828a-513d-45d2-847b-6a09cabfc0f4" contentitemid="64352d31-35b3-4c68-8e70-175fc2b8118b" />
                                    </a>
                                </div>
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3>Tone</h3>
                                    <p>Cleveland Knockouts has partnered with <a href="http://www.nufitcorp.com/">Nu Fit Corp</a> to bring new innovative equipment to northeast Ohio. NuBells fitness equipment will be used by Cleveland Knockouts clients to assure long, lean muscle. Nubells are the replacement for old age dumbbells. They are circular balanced weights with a comfortable safe design.</p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[{&quot;href&quot;:&quot;/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/49f4546a-3f28-4db9-914f-13b8592f025d.jpg&quot;,&quot;title&quot;:&quot;&lt;h3>Tone&lt;/h3>\n&lt;p>Cleveland Knockouts has partnered with &lt;a href=\&quot;http://www.nufitcorp.com/\&quot;>Nu Fit C&quot;,&quot;thumbnail&quot;:&quot;/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/49f4546a-3f28-4db9-914f-13b8592f025d_m.jpg&quot;}]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "765f828a-513d-45d2-847b-6a09cabfc0f4",
                                IsPublish: 1
                            });
                        });
                        </script>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-f19152b0-23f6-4a86-8f70-4d65f9142d0c article-layout-2" sitepagemoduleid="f19152b0-23f6-4a86-8f70-4d65f9142d0c" contentitemid="236fbadf-876a-430c-9350-867ddedc2ccd">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_ImageWrapper" class="article-photo">
                                    <a id="ctl00_ImageLink" class="curs-default">
                                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/7ae321ad-1401-4a6f-bb1b-107e57a1b69b.png" id="ctl00_Image" class="articleImg thumbnail" sitepagemoduleid="f19152b0-23f6-4a86-8f70-4d65f9142d0c" contentitemid="236fbadf-876a-430c-9350-867ddedc2ccd" />
                                    </a>
                                </div>
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3>Box</h3>
                                    <p>Cleveland Knockouts has partnered with <a href="http://rivalboxing.us/" target="_blank">Rival Technical Boxing Gear</a> to give clients fun and effective cardio. We use kickboxing as a fun, strategic way for clients to burn calories and relieve stress. With punches, kicks, elbows, and knees, our clients will see and feel their body progress.</p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[{&quot;href&quot;:&quot;/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/7ae321ad-1401-4a6f-bb1b-107e57a1b69b.png&quot;,&quot;title&quot;:&quot;&lt;h3>Box&lt;/h3>\n&lt;p>Cleveland Knockouts has partnered with &lt;a href=\&quot;http://rivalboxing.us/\&quot; target=\&quot;_bla&quot;,&quot;thumbnail&quot;:&quot;/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/7ae321ad-1401-4a6f-bb1b-107e57a1b69b_m.png&quot;}]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "f19152b0-23f6-4a86-8f70-4d65f9142d0c",
                                IsPublish: 1
                            });
                        });
                        </script>
                    </div>
                </div>
                <div class='row'>
                    <div class='span2'></div>
                    <div class='span8'></div>
                    <div class='span2'></div>
                </div>
            </div>
        </div>
    </div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>