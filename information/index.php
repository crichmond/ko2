<?php
    $page_title="Information - k02.fit";
    $page_description="Over the past 15 years, Michael Delguyd has built a reputation for being a leader and a pioneer in the personal training and fitness industry. Michael began his journey with an industry giant, Bally's Total Fitness. His scorching and limitless desire to impact the fitness industry along with his aristocratic quest for knowledge helped him breeze pa";
    $page_keywords="fit, fitness, gym, workout, crossfit, strong, training, trainer, diet, girl, weight, personal, shape, pumping, stamina, up, strength, background, light, health, biceps, hands,heavy, bodybuilding, sportswoman, muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic, cross-fit,sportswear, sport";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                         <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_divModParallax" class="mod-parallax mod-parallax-95df016f-b5b5-4905-a7a9-e5dee93b24b0 parallax-layout-2 padBg" data-speed="10" sitepagemoduleid="95df016f-b5b5-4905-a7a9-e5dee93b24b0" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" style="max-height: 500px;">
            <img id="ctl00_bgParallaxImg" class="scroll-Parallax-image" sitepagemoduleid="95df016f-b5b5-4905-a7a9-e5dee93b24b0" contentitemid="f9ed4924-e9c9-48ee-9547-2b6847b6e3bc" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            <div>
                <img id="ctl00_invisibleParallaxImg" class="hidden-Parallax-image" sitepagemoduleid="95df016f-b5b5-4905-a7a9-e5dee93b24b0" contentitemid="f9ed4924-e9c9-48ee-9547-2b6847b6e3bc" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            </div>
            <div class="parallax-text-wrap">
                <div class="parallax-text-table">
                    <div id="ctl00_divHeader" class="parallaxtext">
                        <h3 id="ctl00_litParallaxTitle">Information</h3>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["jquery", "imageparallaxv1"], function($, ImageParallaxV1) {
            ImageParallaxV1.loadModule("95df016f-b5b5-4905-a7a9-e5dee93b24b0");
        });
        </script>
    </div>
    <div class='centerContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span4'>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-be4d2245-1add-4201-91b1-9032b15be474 article-layout-1" sitepagemoduleid="be4d2245-1add-4201-91b1-9032b15be474" contentitemid="01bed6a4-e76e-46c0-bdbe-7cb477f13604">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3 class="align-center">Specials</h3>
                                    <p class="align-center">1 session
                                        <br />3 sessions
                                        <br />5 sessions  </p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "be4d2245-1add-4201-91b1-9032b15be474",
                                IsPublish: 1
                            });
                        });
                        </script>
                    </div>
                    <div class='span4'>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-5609d523-59a2-4cfd-bffb-954fa2f1d709 article-layout-1" sitepagemoduleid="5609d523-59a2-4cfd-bffb-954fa2f1d709" contentitemid="d7d9fef4-2082-4a0f-9662-b7c9bb4acebc">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3 class="align-center">1 Session</h3>
                                    <p class="align-center">18 min - Core.
                                        <br />18 min - Tone.
                                        <br />18 min - Box.</p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "5609d523-59a2-4cfd-bffb-954fa2f1d709",
                                IsPublish: 1
                            });
                        });
                        </script>
                    </div>
                    <div class='span4'>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-fca0b805-dcca-4d19-aca5-cae4bec45806 article-layout-1" sitepagemoduleid="fca0b805-dcca-4d19-aca5-cae4bec45806" contentitemid="880368eb-7989-4481-91bf-1f0e655c414e">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h6 class="align-center">BREATHE EASY!</h6>
                                    <h6 class="align-center">TRAIN HARD!</h6>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "fca0b805-dcca-4d19-aca5-cae4bec45806",
                                IsPublish: 1
                            });
                        });
                        </script>
                    </div>
                </div>
                <div class='row'>
                    <div class='span12'>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-9440a3dc-3797-4f3d-922d-c88bf6fe04f2 article-layout-1" sitepagemoduleid="9440a3dc-3797-4f3d-922d-c88bf6fe04f2" contentitemid="f7ec3759-d227-41fa-9bf4-942ade54a2ed">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <h3 class="align-center">Class Times</h3>
                                    <p class="align-center"><span class="bold">5:30am - 6:30am - 10:00am - 12:00pm - 4:30pm - 5:30pm - 6:30pm</span></p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "9440a3dc-3797-4f3d-922d-c88bf6fe04f2",
                                IsPublish: 1
                            });
                        });
                        </script>
                    </div>
                </div>
                <div class='row'>
                    <div class='span2'></div>
                    <div class='span8'>
                        <div id="ctl00_ModArticle" class="mod_article mod_article-7d67d4f5-3250-4fe7-aec8-733eee595c4c article-layout-1" sitepagemoduleid="7d67d4f5-3250-4fe7-aec8-733eee595c4c" contentitemid="7aa8a404-50f3-4bf5-869a-eef3764de99e">
                            <div id="ctl00_divArticleShadow" class="clearfix">
                                <div id="ctl00_BodyWrapper" class="article-desc">
                                    <p class="align-center">*Submit your information and come visit us! See the facility with a fitness jumbo tron that tracks your heart rate and learn about our oxygen enriched environment. Open: 5:30 A.M. - Close: 8:30 P.M. Sessions start: June 2015
                                        <br />*Learn more about Cleveland Knockouts TODAY! Follow us on INSTAGRAM: @CLEVELANDKNOCKOUTS</p>
                                    <div class="art-reward-points">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$hdnImageData" id="hdnImageData" value="[]" />
                        </div>
                        <script type="text/javascript" language="javascript">
                        require(["articlev1"], function(ArticleV1) {
                            var ArticleV1 = new ArticleV1({
                                SitePageModuleID: "7d67d4f5-3250-4fe7-aec8-733eee595c4c",
                                IsPublish: 1
                            });
                        });
                        </script>
                        <form id="ctl00_divModContactForm" class="mod-contact contactform-layout-1" action="/endpoints/contact/" method="POST">
                            <fieldset>
                                <div class="form-container clearfix">
                                    <div id="ctl00_parentheaderdiv" class="mod-header">
                                        <h3 id="ctl00_h3Title" class="form-heading">SET APPOINTMNENT TODAY!</h3>
                                    </div>
                                    <div class="formWrap">
                                        <div class="alert alert-success divContactFormSuccessMessage" style="display: none;">
                                            Success, your message was sent. Thanks!
                                        </div>
                                        <div class="alert alert-error divContactFormFailureMessage" style="display: none;">
                                            Sorry, we failed to send your message!
                                        </div>
                                        <div class="form-left">
                                            <div id="ctl00_divName" class="control-group divName">
                                                <div class="controls">
                                                    <label class="field">
                                                        Name&nbsp;*</label>
                                                    <input name="ctl00$txtName" type="text" id="ctl00_txtName" data-placehold="Type your name" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-parentid="#ctl00_divName" /><span class="help-inline"></span>
                                                </div>
                                            </div>
                                            <div id="ctl00_divEmail" class="control-group divEmail">
                                                <div class="controls">
                                                    <label class="field">
                                                        Email&nbsp;*</label>
                                                    <input name="ctl00$txtEmail" type="text" id="ctl00_txtEmail" data-placehold="Type your address" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-validation="email" data-parentid="#ctl00_divEmail" /><span class="help-inline"></span>
                                                </div>
                                            </div>
                                            <div id="ctl00_divSubject" class="control-group divSubject">
                                                <div class="controls">
                                                    <label class="field">
                                                        Subject&nbsp;*</label>
                                                    <input name="ctl00$txtSubject" type="text" id="ctl00_txtSubject" data-placehold="Type your subject" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-parentid="#ctl00_divSubject" />
                                                    <span class="help-inline"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-right">
                                            <div id="ctl00_divMessage" class="control-group divMessage">
                                                <div class="controls">
                                                    <label class="field">
                                                        Message&nbsp;*</label>
                                                    <textarea name="ctl00$txtMessage" id="ctl00_txtMessage" cols="20" rows="7" data-placehold="Type your message" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-parentid="#ctl00_divMessage"></textarea>
                                                    <span class="help-inline"></span>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="controls-reward-points">
                                                        <a id="ctl00_aSubmit" class="btn btn-primary" data-loading-text="Sending..." autocomplete="off" onclick="javascript:ContactForm.sendEmail(event,this,&#39;294949a7-5896-40e7-aa5e-d8611ff60e5d&#39;);">Send Message </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                        <script type="text/javascript">
                        require(["contactformv1"], function(contactformv1) {
                            ContactForm = contactformv1;
                        });
                        </script>
                    </div>
                    <div class='span2'></div>
                </div>
            </div>
        </div>
    </div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>