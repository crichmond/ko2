<?php
    $page_title="21 day challenge - k02.fit";
    $page_description="What does the +1 Mindset mean? +1 is what happens every time you perform a kind act to make someone else's life better or a little less stressful";
    $page_keywords="training, trainer, diet, girl, weight, personal, shape, pumping, stamina, up, strength, background, light, health, biceps, hands,heavy, bodybuilding, sportswoman, muscles, equipment, figure, triceps, lifestyle, exercising, healthy, holding, body, woman, close, athlete, dumbbells, athletic";
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class=''>
    <div id='mainHeaderContainer' class='headerContainer'>
        <input name="ctl00$hdnSitePageID" type="hidden" id="ctl00_hdnSitePageID" />
        <div id="ctl00_divHeaderModule" class="h-hide header-layout-11">
            <div class="user-action-header">
                <div class="mainContainer">
                    <div class="header-actions-container">
                        <div class="header-actions">
                            <div class="translator">
                                <a id="ctl00_idLang" class="notranslate" onclick="Header.selectLanguage(this)" style="display: none"></a>
                                <div id="divlanguageDropDown" class="dropdown-block language-dropdown notranslate">
                                </div>
                            </div>
                            <div id="ctl00_divCall2Action" class="call2Action">
                                <a id="ctl00_lnkCallToActionLink" class="btn" href="information">(440) 488-2486</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ctl00_divMenu" class="htoggle-menu" onclick="Header.horzToggleClass()" style="display:none;">
                <p class="toggle-text"><span>Menu</span><i class="fa fa-navicon fa-lg mobilePillNav"></i></p>
            </div>
            <div class="divTitleContainer">
                <div id="ctl00_divLogo" class="logoDiv">
                    <a href="/knockouts" id="ctl00_lnkHeaderLogo" target="_self" title="Website Home Page">
                        <img src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/c24bc54e-b483-4e6f-bc0f-b4016ae4dc5d_m.png" id="ctl00_imgHeaderLogo" />
                    </a>
                    <a href="/knockouts" id="ctl00_lnkTitleTxt" target="_self" title="Website home page"></a>
                </div>
            </div>
            <div id="ctl00_navigationBlock" class="h-nav">
                <div class="mainContainer">
                    <div class="subnav">
                         <div class="nav-left">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenu_ctl01_liMenuItem">
                                    <a href="/knockouts" id="ctl00_rptHeaderMenu_ctl01_lnkMenuItem">
    Home
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl03_liMenuItem">
                                    <a href="/about" id="ctl00_rptHeaderMenu_ctl03_lnkMenuItem">
    About
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenu_ctl04_liMenuItem" class="active">
                                    <a href="/21-day-challenge" id="ctl00_rptHeaderMenu_ctl04_lnkMenuItem" class="active">
    21 day challenge
</a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-right">
                            <ul class="nav nav-pills">
                                <li id="ctl00_rptHeaderMenuRight_ctl05_liMenuItem">
                                    <a href="/videos" id="ctl00_rptHeaderMenuRight_ctl05_lnkMenuItem">
    Videos
</a>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl13_liMenuItem" data-event="hover" class="dropdown">
                                    <a href="/social" id="ctl00_rptHeaderMenuRight_ctl13_lnkMenuItem" data-toggle="dropdown" class="dropdown-toggle">
                                    Social
                                    <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class=""><a class="" href="/blog" target="_self">Blog</a></li>
                                    </ul>
                                </li>
                                <li id="ctl00_rptHeaderMenuRight_ctl15_liMenuItem">
                                    <a href="/contact" id="ctl00_rptHeaderMenuRight_ctl15_lnkMenuItem">
    Contact
</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["headerv1"], function(headerv1) {
            Header = headerv1;
            Header.StoreID = "";
            Header.initEl('ctl00_divHeaderModule', '0', '0');
            Header.loadSelectik();
            Header.SitePageUrls = "";
            Header.loadShoppingJS('', '1063');
            Header.IsPublish = 1;
        });
        </script>
        <div class="mod-embed-iframe embed-layout-1">
            <div class="mod-embed-iframe">
                <style>
                .btn {
                    background: #5eeb3b!important;
                    background-color: #5eeb3b!important;
                    border-color: #fff!important;
                }

                .btn:hover {
                    color: #000!important;
                    background: #ccc!important;
                    background-color: #ccc!important;
                    border-color: #5eeb3b!important;
                }
                </style>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class='wideContainer noBG'>
        <div id="ctl00_divModParallax" class="mod-parallax mod-parallax-f49a0a50-6451-4180-bc88-8b3e8e2372d9 parallax-layout-2 padBg" data-speed="10" sitepagemoduleid="f49a0a50-6451-4180-bc88-8b3e8e2372d9" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" style="max-height: 500px;">
            <img id="ctl00_bgParallaxImg" class="scroll-Parallax-image" sitepagemoduleid="f49a0a50-6451-4180-bc88-8b3e8e2372d9" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            <div>
                <img id="ctl00_invisibleParallaxImg" class="hidden-Parallax-image" sitepagemoduleid="f49a0a50-6451-4180-bc88-8b3e8e2372d9" contentitemid="5ff4fb74-16ca-4ea0-847a-d95c63985218" src="/assets-cloudfront/35C5F64E-D137-45E8-A1E4-83D0CF1E97FB/a93e8aa2-d2c0-43b1-9052-25cb1e0c1665_h.jpg" border="0" />
            </div>
            <div class="parallax-text-wrap">
                <div class="parallax-text-table">
                    <div id="ctl00_divHeader" class="parallaxtext">
                        <h3 id="ctl00_litParallaxTitle">21 day challenge</h3>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        require(["jquery", "imageparallaxv1"], function($, ImageParallaxV1) {
            ImageParallaxV1.loadModule("f49a0a50-6451-4180-bc88-8b3e8e2372d9");
        });
        </script>
    </div>
    <div class='centerContainer'>
        <div class='mainContainer'>
            <div class='container'>
                <div class='row'>
                    <div class='span12'>
                        <link rel="Stylesheet" type="text/css" href="/assets-cloudfront/moprosuite/v10/uikit/_static/_css/jquery.picker.default.css" />



                        <?php /* ----------------- ORIGINAL FORM ------------------
                        <div id="ctl00_parentmodheaderdiv" class="mod-form-builder mod-form-builder-e5df6ae1-3fac-428c-b437-927b65037110 form-builder-layout-1">
                            <div id="ctl00_parentheaderdiv" class="mod-header">
                                <h6 id="ctl00_h6Subtitle">Who can burn the most calories in 21 days? Start by filling out this form.</h6>
                            </div>
                            <div class="row mod-form-builder-info">
                                <div class="alert alert-success hide">
                                    <button type="button" class="close" data-dismiss="alert">
                                        ×</button>
                                </div>
                                <div class="alert alert-error hide">
                                    <button type="button" class="close" data-dismiss="alert">
                                        ×</button>
                                </div>
                            </div>
                            <div class="form">
                                <div class="row">
                                    <label>Name</label>
                                    <input id="czr8jYB7N06kTQurIuu6cQ" type="text" />
                                </div>
                                <div class="row">
                                    <label>Email</label>
                                    <input id="8NuwMnYELEC4mrwdHcXa7g" type="text" autocomplete="off" />
                                </div>
                                <div class="row">
                                    <label>Phone</label>
                                    <input id="pVKfHl-E3U6hgDXcxzbFPQ" type="text" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="art-reward-points">
                                    <a href="javascript:void(0);" id="ctl00_aFormSubmit" class="btn btn-primary save pull-left" title="Submit">
Submit</a>
                                    <div class="loading pull-left">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="ctl00$fields" id="ctl00_fields" value="[{&quot;required&quot;:false,&quot;id&quot;:16771,&quot;field&quot;:&quot;czr8jYB7N06kTQurIuu6cQ&quot;},{&quot;required&quot;:false,&quot;id&quot;:17121,&quot;field&quot;:&quot;8NuwMnYELEC4mrwdHcXa7g&quot;},{&quot;required&quot;:false,&quot;id&quot;:16771,&quot;field&quot;:&quot;pVKfHl-E3U6hgDXcxzbFPQ&quot;}]" />
                        </div>
                        <script type="text/javascript">
                        require(["formbuilderv1"], function(FormBuilderV1) {
                            new FormBuilderV1({
                                SitePageModuleID: "e5df6ae1-3fac-428c-b437-927b65037110",
                                lang: "en",
                                RecaptchaPublicKey: ""
                            }).render();
                        });
                        </script> */ ?>


                        <form id="ctl00_divModContactForm" class="mod-contact contactform-layout-4" action="/endpoints/21-day-challenge/" method="POST">
                            <fieldset>
                                <div class="form-container clearfix">
                                    <div id="ctl00_parentheaderdiv" class="mod-header">
                                        <h3 id="ctl00_h3Title" class="form-heading" style="font-size:1em;line-height:1.2em;margin-bottom:1em;">Who Can Burn The Most Calories In 21 Days? Start By Filling Out This Form.
</h3>
                                    </div>
                                    <div class="formWrap">
                                        <div class="alert alert-success divContactFormSuccessMessage" style="display: none;">
                                            Success, your message was sent. Thanks!
                                        </div>
                                        <div class="alert alert-error divContactFormFailureMessage" style="display: none;">
                                            Sorry, we failed to send your message!
                                        </div>
                                        <div class="form-left">
                                            <div id="ctl00_divName" class="control-group divName">
                                                <div class="controls">
                                                    <label class="field">
                                                        Name&nbsp;*</label>
                                                    <input name="ctl00$txtName" type="text" id="ctl00_txtName" data-placehold="Type your name" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-parentid="#ctl00_divName" /><span class="help-inline"></span>
                                                </div>
                                            </div>
                                            <div id="ctl00_divEmail" class="control-group divEmail">
                                                <div class="controls">
                                                    <label class="field">
                                                        Email&nbsp;*</label>
                                                    <input name="ctl00$txtEmail" type="text" id="ctl00_txtEmail" data-placehold="Type your address" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-validation="email" data-parentid="#ctl00_divEmail" /><span class="help-inline"></span>
                                                </div>
                                            </div>
                                            <div id="ctl00_divSubject" class="control-group divSubject">
                                                <div class="controls">
                                                    <label class="field">
                                                        Phone&nbsp;*</label>
                                                    <input name="ctl00$txtSubject" data-validation="phone" type="tel" id="ctl00_txtSubject" data-placehold="Type your phone number" onfocus="ContactForm.removeError(this);" data-required="true" data-error="Please fill in the required field." data-parentid="#ctl00_divSubject" />
                                                    <span class="help-inline"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-right">
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="controls-reward-points">
                                                        <a id="ctl00_aSubmit" class="btn btn-primary" data-loading-text="Sending..." autocomplete="off" onclick="javascript:ContactForm.sendEmail(event,this,&#39;294949a7-5896-40e7-aa5e-d8611ff60e5d&#39;);">Submit </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                        <script type="text/javascript">
                        require(["contactformv1"], function(contactformv1) {
                            ContactForm = contactformv1;
                        });
                        </script>



                        <div class="mod-embed-iframe embed-layout-1">
                            <div class="mod-embed-iframe">
                                <style>
                                .mod-form-builder label {
                                    font-family: inherit;
                                    font-size: 19px;
                                    color: #000000 !important;
                                }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include $_SERVER['DOCUMENT_ROOT'] . '/inc/footer.php'; ?>